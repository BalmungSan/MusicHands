<?include ("checkUser.php");?>

<?php

$username = $_SESSION["username"];
$userID = $_SESSION["userid"];

$con = new mysqli("mysql9.000webhost.com", "a4312417_root", "josalu16", "a4312417_modules");

if($con->connect_errno){
	echo "<script>alert('Error with the database, please try again later'); window.location.href='index.php';</script>";
}

    //Get the userID of the given username and password

$sql= "SELECT * FROM Modules";

if(!$result = $con->query($sql)){
	echo "<script>alert('Error with the database, please try again later'); window.location.href='index.php';</script>";
}

$sqlUser= "SELECT Balance FROM Users WHERE UserName = '$username'";

if(!$result2 = $con->query($sqlUser)){
	echo "<script>alert('Error with the database, please try again later'); window.location.href='index.php';</script>";
}

$sqlModuleUses = "SELECT RemainingUses, Modules_ModuleID FROM ModulesPurchased WHERE Users_UserID = '$userID'";

if(!$result3 = $con->query($sqlModuleUses)){
	echo "<script>alert('Error with the database, please try again later'); window.location.href='index.php';</script>";
}

$usosRestantes = array();

while($row = $result3->fetch_row()){
	$usosRestantes[$row[1]] = $row[0];
}

?>

<!DOCTYPE html>

<html lang="es"><head>

<meta name="viewport" content="width=device-width, initial-scale=1">

<meta http-equiv="content-type" content="text/html; charset=UTF-8">

<title>E-Store Modules | Music Hands</title>

<meta charset="utf-8">



<!-- Linking styles -->

<link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">

<link rel="stylesheet" href="css/style_store.css" type="text/css" media="screen">

<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<link rel="shortcut icon" href="images/favicon.png">

<!-- Linking scripts -->
<script src="https://code.jquery.com/jquery-2.2.3.min.js" integrity="sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo=" crossorigin="anonymous"></script> 

<script src="js/main.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

</head>

<body>

	<div class="container">

		<header><!-- Defining the header section of the page -->

			<div class="top_head"><!-- Defining the top head element -->

			<?php
				$row = $result2->fetch_row();
				$saldo = $row[0];
			?>
				<div id="salir">
					<a id="saldo">Tu Saldo Actual es: $<?=$saldo?></a>
					<button type="button" class="btn btn-danger" onclick="window.location.href='salir.php';"><strong>Log Out</strong></button>
				</div>

				<div class="logo"><!-- Defining the logo element -->

					<img id="logo" src="images/logo.jpg">

				</div>
			</div>

		</header>


		<div id="main"><!-- Defining submain content section -->

			<section id="content"><!-- Defining the content section #2 -->

				<div id="left">

					<h3>Last Modules</h3>

					<ul>

						<?php

						$moduleNum = 1;

						while($row = $result->fetch_row()){

							if(array_key_exists("$row[0]", $usosRestantes)){
								$numUsos = $usosRestantes[$row[0]];
							}else{
								$numUsos = 0;
							}

							?>

							<li>

								<div class="img"><a href="#"><img alt="" src="data:image/jpeg;base64,<?=base64_encode($row[3])?>"/></a></div>

								<div class="info">

									<a class="title"><?=utf8_encode($row[1])?></a>

									<div style="display: none;" id="info<?=$moduleNum?>">

										<p><?=utf8_encode($row[2])?></p>

										<audio class="player" controls="controls">

											<source src="sounds/sound<?=$moduleNum?>.wav" type="audio/wav"/>

											Your browser does not support the audio element.

										</audio>

										<p style="font-size: 17px; margin-top: 15px;"><span class="st">Our price: </span><strong>$<?=$row[4]?></strong></p>

										<p style="font-size: 17px; margin-top: 15px;"><span class="st">Usos Restantes: </span><strong><?=$numUsos?></strong></p>

										<?php if($numUsos > 0) : 
										$leftCode = str_pad($userID, 4, "0", STR_PAD_LEFT);
										$rightCode = str_pad($row[0], 4, "0", STR_PAD_LEFT);
										$codigo = "$leftCode-$rightCode";
										?>
											<p style="font-size: 17px; margin-top: 15px;"><span class="st">El Código es: </span><strong><?=$codigo?></strong></p>
										<?php endif; ?>

									</div>

									<div class="actions">

										<button type="button" class="btn btn-info details" onclick="return description('info<?=$moduleNum?>')"><strong>Details</strong></button>

										<button id="cart<?=$moduleNum?>" type="button" class="btn btn-success cart" onclick="window.location.href='buyModules.php?id=<?=$row[0]?>';"><strong>Comprar</strong></button>

									</div>

								</div>

							</li>

							<?php

							$moduleNum++;

						}

						?>

					</ul>


				</div>

				<div id="right">

					<h3>Top sells</h3>

					<ul>

						<?php

						$moduleNum = 1;

						$sql= "SELECT * FROM Modules";

						if(!$result = $con->query($sql)){

							echo "<script>alert('Error with the database, please try again later'); window.location.href='index.php';</script>";

						}

						while($row = $result->fetch_row()){

							?>

							<li>

								<div class="img"><a href="#"><img alt="" src="data:image/jpeg;base64,<?=base64_encode($row[3])?>"></a></div>

								<div class="info">

									<a class="title"><?=utf8_encode($row[1])?></a>

									<div class="price">

										<span class="usual">$<?php $precio=$row[4]+50; echo $precio?> </span>&nbsp;

										<span class="special">$<?=$row[4]?></span>

									</div>

								</div>

							</li>

							<?php

							$moduleNum++;

						}

						$con->close();

						?>

					</ul>

				</div>

			</section>

		</div>


		<footer><!-- Defining the footer section of the page -->

			<div id="privacy">

				E-Store Music Hands © 2016

			</div>

		</footer>



	</div>

</body>
</html>