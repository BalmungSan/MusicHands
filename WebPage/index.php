<!DOCTYPE html>

<html lang="es" class="no-js"> 

<head>
    <meta charset="UTF-8" />
    <title>Login and Registration Music Hands</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <link rel="shortcut icon" href="images/favicon.ico"> 
    <link rel="stylesheet" type="text/css" href="css/demo.css" />
    <link rel="stylesheet" type="text/css" href="css/style_login.css" />
    <link rel="stylesheet" type="text/css" href="css/animate-custom.css" />

</head>
<body>
    <div class="container">
        <header>

            <h1>Login and Registration <span>Music Hands</span></h1>

        </header>

        <section>
            <div id="container_demo" >
                <a class="hiddenanchor" id="toregister"></a>
                <a class="hiddenanchor" id="tologin"></a>
                <div id="wrapper">
                    <div id="login" class="animate form">

                        <form  autocomplete="on" id="form_id" method="post" name="myform" action="login.php"> 
                            <h1>Log in</h1> 
                            <p> 
                                <label for="username" class="uname" data-icon="u" > Your email or username </label>
                                <input id="username" name="username" required="required" type="text" placeholder="myusername"/>
                            </p>
                            <p> 
                                <label for="password" class="youpasswd" data-icon="p"> Your password </label>

                                <input id="password" name="password" required="required" type="password" placeholder="eg. X8df!90EO" /> 
                            </p>

                            <p class="login button"> 
                                <input type="submit" name="Submit" value="Login"/> 
                            </p>
                            <p class="change_link">
                             Not a member yet ?
                                <a href="#toregister" class="to_register">Join us</a>
                            </p>
                        </form>
                    </div>


                     <div id="register" class="animate form">

                        <form  id="register_form_id" method="post" name="myform" autocomplete="on" action="register.php"> 

                            <h1> Sign up </h1> 
                            <p> 
                                <label for="usernamesignup" class="uname" data-icon="u">Your username</label>
                                <input id="usernamesignup" name="usernamesignup" required="required" type="text" placeholder="mysuperusername690" />
                            </p>

                            <p> 
                                <label for="passwordsignup" class="youpasswd" data-icon="p">Your password </label>
                                <input id="passwordsignup" name="passwordsignup" required="required" type="password" placeholder="eg. X8df!90EO"/>
                            </p>


                            <p> 
                                <label for="passwordsignup_confirm" class="youpasswd" data-icon="p">Please confirm your password </label>
                                <input id="passwordsignup_confirm" name="passwordsignup_confirm" required="required" type="password" placeholder="eg. X8df!90EO"/>
                            </p>

                            <p class="signin button"> 
                               <input type="submit" value="Sign up" /> 
                           </p>
                           <p class="change_link">  
                               Already a member ?
                               <a href="#tologin" class="to_register"> Go and log in </a>
                           </p>
                       </form>
                   </div> 
               </div>
           </div>
       </section>
    </div>
</body>
</html>