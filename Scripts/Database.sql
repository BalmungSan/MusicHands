-- -----------------------------------------------------
-- MUSICH HANDS DATABASE
-- AUTHOR: Luis Miguel Mejía Suárez
-- VERSION: 3.0.0-THIRD_RELEASE (17/05/2016)
-- -----------------------------------------------------

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


-- -----------------------------------------------------
-- Table `Users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Users`;
CREATE TABLE IF NOT EXISTS `Users` (
  `UserID` INT NOT NULL AUTO_INCREMENT,
  `UserName` NVARCHAR(45) NOT NULL,
  `UserPassword` BINARY(32) NOT NULL COMMENT 'MD5 hash',
  `Balance` INT NOT NULL DEFAULT 0 COMMENT 'The amount of coins the user have',
  PRIMARY KEY (`UserID`),
  UNIQUE INDEX `UserName_UNIQUE` (`UserName` ASC),
  UNIQUE INDEX `idUser_UNIQUE` (`UserID` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Modules`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Modules`;
CREATE TABLE IF NOT EXISTS `Modules` (
  `ModuleID` INT NOT NULL AUTO_INCREMENT,
  `ModuleName` NVARCHAR(40) NOT NULL,
  `ModuleDescription` TINYTEXT NOT NULL,
  `ModuleImage` MEDIUMBLOB NOT NULL COMMENT 'The icon of the module',
  `ModulePrice` INT NOT NULL,
  `ModuleLink` NVARCHAR(255) NOT NULL COMMENT 'The download link of the module',
  PRIMARY KEY (`ModuleID`),
  UNIQUE INDEX `idModule_UNIQUE` (`ModuleID` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ModulesPurchased`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ModulesPurchased` ;
CREATE TABLE IF NOT EXISTS `ModulesPurchased` (
  `Users_UserID` INT NOT NULL,
  `Modules_ModuleID` INT NOT NULL,
  `PurchaseDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The date and hour of purchase',
  `RemainingUses` INT NOT NULL DEFAULT 3,
  PRIMARY KEY (`Users_UserID`, `Modules_ModuleID`),
  INDEX `fk_Users_has_Modules_Modules1_idx` (`Modules_ModuleID` ASC),
  INDEX `fk_Users_has_Modules_Users_idx` (`Users_UserID` ASC),
  CONSTRAINT `fk_Users_has_Modules_Users`
    FOREIGN KEY (`Users_UserID`)
    REFERENCES `Users` (`UserID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Users_has_Modules_Modules1`
    FOREIGN KEY (`Modules_ModuleID`)
    REFERENCES `Modules` (`ModuleID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Procedure `downloadModule`
-- This procedure check if the user identified by userID
-- can download the module identified by moduleID
-- if he cans, returns the link to the module in result
-- if not, returns an error
-- -----------------------------------------------------
/*
DROP PROCEDURE IF EXISTS `downloadModule`;
DELIMITER $$
CREATE PROCEDURE `downloadModule` (IN userID INT, IN moduleID INT, OUT result NVARCHAR(255))
BEGIN
	-- Get the remaining  uses for this user to this module
	SELECT @remainingUses = RemainingUses FROM ModulesPurchased
    WHERE Users_UserID = userID AND Modules_ModuleID = moduleID;
    
    -- Check if the user have more uses ...
    IF (@remainingUses > 0) THEN
		-- ... if he has take one
		SET @remainingUses = @remainingUses - 1;
        
        -- update the number of remaining uses
        UPDATE ModulesPurchased SET RemainingUses = @remainingUses
        WHERE Users_UserID = userID AND Modules_ModuleID = moduleID;
        
        -- get the link to the module
		SELECT ModuleLink AS link FROM Module WHERE ModuleID = moduleID;
        
        -- return the link
        SET result = link;
    ELSE
		-- .. if not return an error
		SET result = "ERROR: No more uses!";
    END IF;
END$$
DELIMITER ;
*/


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
