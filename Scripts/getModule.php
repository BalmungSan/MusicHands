<?php
/**
 * This script is used to get the ftp link of a module
 * Author: Luis Miguel Mejía Suárez
 */
 
//Check if the data was set
if(isset($_POST["data"])) {
	//Get the parametres
	$data = $_POST["data"];
	$userID = substr($data, 0, strpos($data, "-"));
	$moduleID = substr($data, strpos($data, "-") + 1);
	
	//Connect to the database
	$con = new mysqli("mysql9.000webhost.com", "a4312417_root", "josalu16", "a4312417_modules");
	if ($con->connect_errno) {
	   //If the connection fails, exit with an error message
	   exit("Failed to connect to MySQL: " . $con->connect_error);
	}
	
	//Get the remaining  uses for this user to this module
	$sql = "SELECT RemainingUses FROM ModulesPurchased WHERE Users_UserID = " . $userID . " AND Modules_ModuleID = " . $moduleID;
	if (!$result = $con->query($sql)) {
		//If the query fails, exit with an error message
		exit("ERROR: Getting the remaining uses of the module\n" . $con->error);
	} else {
		//Check if the user have more uses ...
		$remainingUses = $result->fetch_row();
		$remainingUses = $remainingUses[0];
		if($remainingUses > 0) {
			//Update the number of remaining uses
			$remainingUses--;
			$sql = "UPDATE ModulesPurchased SET RemainingUses = " . $remainingUses . " WHERE Users_UserID = " . $userID . " AND Modules_ModuleID = " . $moduleID;
			if (!$result = $con->query($sql)) {
				//If the query fails, exit with an error message
				exit("ERROR: Updating the remaining uses of the module\n" . $con->error);
			} else {
				//Get the link to the module
				$sql = "SELECT ModuleLink FROM Modules WHERE ModuleID = " . $moduleID;
				if (!$result = $con->query($sql)) {
					//If the query fails, exit with an error message
					exit("ERROR: Getting the link of the module\n" . $con->error);
				} else {
					//Return the link
					$link = $result->fetch_row();
					echo $link[0];
				}
			}
		} else {
			//If not, exit with an error message
			exit("ERROR: No more uses\n" . $con->error);
		}
	}
} else {
	//If not, exit with an error message
	exit("ERROR: receiving the data\n");
}
?>