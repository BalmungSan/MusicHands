Music Hands: The idea consists in the realization of a device which is placed on the user's hands
             in order to sense the different positions of the user hands to subsequently
             send this information to a program that has the ability to interpret this data
             and perform various actions that were previously set by the user

 -- Authors:
   -- Alejandro Salgado G�mez
   -- Luis Miguel Mej�a Su�rez
   -- Johan Sebastian Yepes Rios

 -- Version: 3.0.0-FINAL_RELEASE (06/06/2016)

 -- Files:
  -- Glove: Arduino project folder, with the source code of the arduino sketch that runs in the gloves hardware
    -- glove.ino: File with the main code of the arduino sketch to detect the hands movements of the user
    -- bluetooth.ino: File with the code to send data via bluetooth

  -- GloveTest: This folder contains a test for the arduino code written in C
    -- test.c: The arduino test

  -- Music Hands: Maven project folder, with the java source code for the pc application

  -- Installer: Maven project folder, with the java source code for the module/profile installer

  -- Scripts: In this folder are all the scripts (PHP and SQL) needed for the application store
     -- Database.sql: MySQL script to create the database of the store.
     -- getModule.php: PHP script to get the link of a module given its purchase code

  -- WebPage: In this folder are all files of the application store webpage
