#include <stdio.h>

///////////////////////////////////////////
int readed[] = {0,0,0,0,0,0,0,0,0,7,8,9,7,7};

int digitalRead(int pos){
    return readed[pos-5];
}

int analogRead(int pos){
    return readed[pos+9];
}
///////////////////////////////////////////

/*******************************/
void send(int code){
    printf("Sended: %d\n", code);
}
/*******************************/

const short int flxIdx = 0;
const short int flxMid = 1;
const short int flxRng = 2;

const short int flxIdxDw = 1;
const short int flxIdxUp = 2;

const short int flxMidDw = 3;
const short int flxMidUp = 4;

const short int flxRngDw = 5;
const short int flxRngUp = 6;

const short int flxIdxDwVal = 10;
const short int flxIdxUpVal = 5;

const short int flxMidDwVal = 11;
const short int flxMidUpVal = 6;

const short int flxRngDwVal = 12;
const short int flxRngUpVal = 7;

//-------------------------------------//

const short int joyX = 0;
const short int joyY = 1;

const short int joyUp = 1;
const short int joyDw = 3;

const short int joyRg = 2;
const short int joyLf = 4;

const short int joyUpVal = 5;
const short int joyDwVal = 10;

const short int joyRgVal = 10;
const short int joyLfVal = 5;

//-------------------------------------//

//       hand    ----|----|----

int btnPin[9] =   {5, 6, 7,     // Index
                   8, 9, 10,    // Middle
                   11, 12, 13}; // Ring

int btnLast[9] =  {0,  0,  0,   // Index
                   0,  0,  0,   // Middle
                   0,  0,  0};  // Ring

int btnCode[9] =  {0,  1,  2,   // Index
                   3,  4,  5,   // Middle
                   6,  7,  8};  // Ring

//-------------------------------------//

int flxPin[3] =   {0,       // Index
                   1,       // Middle
                   2};      // Ring

int flxLast[3] =  {0,       // Index
                   0,       // Middle
                   0};      // Ring

//                 DW  UP

int flxCode[6] =  {9,  10,  // Index
                   11, 12,  // Middle
                   13, 14}; // Ring

//-------------------------------------//

int joyPin[2] =   {3,       // X axis
                   4};      // Y axis

int joyLast[2] =  {0,       // X axis
                   0};      // Y axis

//                 UP   RG
//                 --   --
//                 DW   LF

int joyCode[4] =  {15,  16,
                   17,  18};

//-------------------------------------//

int lastStateBtn(int btn, int ret){
    int tmp = btnLast[btn];
    btnLast[btn] = ret;
    return tmp;
}

void button(int btn){
    int ret = digitalRead(btnPin[btn]);
    int code = btnCode[btn];

    if(lastStateBtn(btn, ret) == 0 && ret) send(code);
}

int parseFlxIdxRet(int ret){
    if(ret > flxIdxDwVal) return flxIdxDw;
    else if(ret < flxIdxUpVal) return flxIdxUp;
    else return 0;
}

int parseFlxMidRet(int ret){
    if(ret > flxMidDwVal) return flxMidDw;
    else if(ret < flxMidUpVal) return flxMidUp;
    else return 0;
}

int parseFlxRngRet(int ret){
    if(ret > flxRngDwVal) return flxRngDw;
    else if(ret < flxRngUpVal) return flxRngUp;
    else return 0;
}

int getFlxCode(int flx, int val){
    switch(flx){
        case 0:
            if(val == 1) return flxCode[0];
            else return flxCode[1];
        case 1:
            if(val == 3) return flxCode[2];
            else return flxCode[3];
        case 2:
            if(val == 5) return flxCode[4];
            else return flxCode[5];
    }
}

int lastStateFlx(int flx, int ret){
    int tmp = flxLast[flx];
    flxLast[flx] = ret;
    return tmp;
}

void flexIdx(){
    int ret = analogRead(flxPin[flxIdx]);
    int val = parseFlxIdxRet(ret);

    if(lastStateFlx(flxIdx, val) == 0 && (val == flxIdxUp || val == flxIdxDw))
        send(getFlxCode(flxIdx, val));
}

void flexMid(){
    int ret = analogRead(flxPin[flxMid]);
    int val = parseFlxMidRet(ret);

    if(lastStateFlx(flxMid, val) == 0 && (val == flxMidUp || val == flxMidDw))
        send(getFlxCode(flxMid, val));
}

void flexRng(){
    int ret = analogRead(flxPin[flxRng]);
    int val = parseFlxRngRet(ret);

    if(lastStateFlx(flxRng, val) == 0 && (val == flxRngUp || val == flxRngDw))
        send(getFlxCode(flxRng, val));
}

int parseJoyXRet(int ret){
    if(ret < joyLfVal) return joyLf;
    else if(ret > joyRgVal) return joyRg;
    else return 0;
}

int parseJoyYRet(int ret){
    if(ret > joyDwVal) return joyDw;
    else if(ret < joyUpVal) return joyUp;
    else return 0;
}

int lastStateJoy(int joy, int ret){
    int tmp = joyLast[joy];
    joyLast[joy] = ret;
    return tmp;
}

int getJoyCode(int joy, int val){
    switch(joy){
        case 0:
            if(val == 4) return joyCode[3];
            else return joyCode[1];
        case 1:
            if(val == 1) return joyCode[0];
            else return joyCode[2];
    }
}

void joystickX(){
    int ret = analogRead(joyPin[joyX]);
    int val = parseJoyXRet(ret);

    if(lastStateJoy(joyX, val) == 0 && (val == joyLf || val == joyRg))
        send(getJoyCode(joyX, val));
}

void joystickY(){
    int ret = analogRead(joyPin[joyY]);
    int val = parseJoyYRet(ret);

    if(lastStateJoy(joyY, val) == 0 && (val == joyDw || val == joyUp))
        send(getJoyCode(joyY, val));
}

void loop(){
    for(int i=0;i<9;i++) button(i);
    flexIdx();
    flexMid();
    flexRng();
    joystickX();
    joystickY();
}

////////////////////////////////////////////
int main(int argc, char *argv[]){

    printf("Buttons:                Flex:                          Joystick:\n\n");
    printf("    0: index bottom         9: index                       12: x axis\n");
    printf("    1: index middle             down > 10 ; send 9              left > 10  ; send 16\n");
    printf("    2: index top                up < 5    ; send 10             right < 5  ; send 18\n\n");

    printf("    3: middle bottom        10: middle                     13: y axis\n");
    printf("    4: middle middle            down > 11  ; send 11             down > 10  ; send 17\n");
    printf("    5: middle top               up < 6     ; send 12             up < 5     ; send 15\n\n");

    printf("    6: ring bottom          11: ring\n");
    printf("    7: ring middle              down > 12 ; send 13\n");
    printf("    8: ring top                 up < 7    ; send 14\n");
    printf("    off = 0, on = 1\n\n");

    fflush(stdout);

    int pos;
    int data;

    while(scanf("%d %d", &pos, &data) > 0){
        readed[pos] = data;
        loop();

        fflush(stdout);
    }

    return 0;
}
////////////////////////////////////////////
