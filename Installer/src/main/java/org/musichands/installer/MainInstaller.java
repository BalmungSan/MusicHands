/**
 * Music Hands - Installer: Java application to install modules to MusicHands
 *
 * @author Luis Miguel Mejía Suárez
 * @version 1.0-SNAPSHOT (09/05/2016)
 */

package org.musichands.installer;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;

/**
 * Main class and only class of this porject
 * All the code logic is run in he Main Method
 * @author Luis Miguel Mejía Suárez
 */
public class MainInstaller {
  /**
   * Application entry point
   * This method install everything in installationPath
   * to the MusicHands jar and delete the temp folder
   * @param args the command line arguments
   * java -jar Installer.jar |MusicHands.jar |actulDir |installationPath
   *                         |args[0]        |args[1]  |args[2]
   */
  public static void main(String[] args) {
    try {
      Thread.sleep(1000);
        
      //Prepare and execute the update command.
      //actualDir/installationPath> jar uf MusicHands.jar *
      String commands[] = new String[] {"jar", "uf", args[0], "*"};
      Runtime.getRuntime().exec(commands, null,
                                new File(args[1] + args[2])).waitFor();

      //Delete the temporary folder
      FileUtils.deleteDirectory(new File(args[1] + "/temp"));

      //Relaunch the main application
      //java -jar MusicHands.jar
      commands = new String[]{"java", "-jar", args[0]};
      Runtime.getRuntime().exec(commands);
    } catch (IOException | InterruptedException ex) {
      //Print the error, should be changed for logger
      System.err.println("ERROR: " + ex.getMessage());
    } finally {
      //Finally exit
      System.exit(0);
    }
  }
}
