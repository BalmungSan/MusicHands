/**
   This file is in charge of connect the glove with the pc application via wifi
   @author: Johan Sebastian Yepes Rios
*/

#include <Bridge.h>
#include <BridgeServer.h>
#include <BridgeClient.h>

BridgeServer server(65000);
BridgeClient client;
int rec;

/**
   wifi setup method, runs once before start the execution of the program
   Establish a wifi connection between the glove and the pc application
*/
void wifi_setup () {
  // Bridge startup
  Bridge.begin();
  // Listen for incoming connection
  server.noListenOnLocalhost();
  server.begin();
  do {
    client = server.accept();
  } while (!client);
  Serial.println("Iniciando Ejecucion");
}

/**
   This method receive a signal and send it via wifi to the pc application
   @param data integer number between 0 to 14; associated to the movement that the glove has detected
*/
void sendData (int data) {
  client.println(data);
}

void tryClose () {
  rec = client.read();
  if (rec == 48) {
    client.stop();
  }
}

