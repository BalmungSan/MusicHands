/**
 * Music Hands: The idea consists in the realization of a device which
 *              is placed on the user's hands in order to sense the different
 *              positions of the user hands to subsequently send this
 *              information to a program that has the ability to interpret
 *              this data and perform various actions that were previously
 *              set by the user
 *
 * @author Alejandro Salgado Gómez
 * @author Luis Miguel Mejía Suárez
 * @author Johan Sebastian Yepes Rios
 *
 * @version 3.1.0-FINAL_RELEASE (06/06/2016)
 */

package org.musichands.musichands;

import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.lang.reflect.InvocationTargetException;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import org.musichands.musichands.backend.Controller;
import org.musichands.musichands.gui.MainInterface;

/**
 * Main class, Here the program is initialized
 * @author Luis Miguel Mejía Suárez
 */
public class MainMusicHands {
  /**
   * Main method all the code logic runs here
   * @param args arguments passed to the program
   */
  public static void main (String[] args) {
    //Enable the Nimbus Look and Feel -------------------------------------
    try {
      UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
    } catch (ClassNotFoundException | InstantiationException |
             IllegalAccessException | UnsupportedLookAndFeelException ex) {
      System.err.println("Can not Enable Nimbus Interface Style "
                         + ex.getLocalizedMessage());
    }
    //---------------------------------------------------------------------

    //Start the program ----------------------------------------------------
    //Establish a connection with the arduino (glove)
    Controller.start();

    //Start the gui in a new thread in the EDT
    try {
      SwingUtilities.invokeAndWait(new Runnable() {
          @Override
          public void run() {
            JFrame gui = new JFrame();
            gui.add(MainInterface.getInstance());
            gui.setMinimumSize(new Dimension(800, 600));
            gui.setSize(800, 600);
            gui.setTitle("MUSIC HANDS");

            //Create our own WindowAdpater to override the method windowClosing
            gui.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent event) {
                  gui.setVisible(false);
                  gui.dispose();
                  Controller.stop();
                }
              });

            gui.setVisible(true);

            //If the controller isn't connected, activate the no connection mode
            if (!Controller.isConnected())
              MainInterface.getInstance().noConnection();
          }
        });
    } catch (InterruptedException | InvocationTargetException ex) {
      System.err.println("Error creating the gui: ");
      ex.printStackTrace(System.err);
      System.exit(1);
    }
    //---------------------------------------------------------------------
  }
}
