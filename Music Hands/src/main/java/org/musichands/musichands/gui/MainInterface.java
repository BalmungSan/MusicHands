package org.musichands.musichands.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import org.musichands.musichands.backend.Controller;
import org.musichands.musichands.movements.MovementID;
import org.musichands.musichands.movements.actions.Action;

/**
 * This class is the main JPanel of the application gui
 * This class is a singleton
 * @author Johan Sebastián Yepes Rios
 * @see JPanel
 * @see ActionListener
 */
public class MainInterface extends JPanel implements ActionListener {
  //The unique instance of this class
  private static volatile MainInterface INSTANCE = null;

  /**
   * @return The instance of this class
   */
  public static synchronized MainInterface getInstance () {
    if (INSTANCE == null) INSTANCE = new MainInterface();
    return INSTANCE;
  }

  //The panel for select actions
  private final ActionsSelectorPanel actionsSelectorPanel;

  //The panel with all the movements
  private final MovementsPanel movementsPanel;

  //This button change the state of the program pause <-> play
  private final JButton play;

  /**
   * Constructor of the class, only call once in the application startup
   * Initialice all the components of the gui
   * @see MovementsPanel
   * @see ActionsSelectorPanel
   * @see JPanel
   */
  private MainInterface () {
    //Call the constructor of my parent (JPanel) with a new BorderLayout
    super(new BorderLayout(5, 5));

    //Initialice the actions selector panel
    this.actionsSelectorPanel = new ActionsSelectorPanel(this);
    actionsSelectorPanel.setBorder(BorderFactory.createLoweredBevelBorder());

    //Initialice the movements panel
    this.movementsPanel = new MovementsPanel(this, Controller.getAllProfiles());
    movementsPanel.setBorder(BorderFactory.createLoweredBevelBorder());

    //Add the pasue/play button
    ImageIcon icon = new ImageIcon
      (new ImageIcon(this.getClass()
                     .getResource("/resources/images/pause-play.png"))
       .getImage().getScaledInstance(20, 20, Image.SCALE_SMOOTH));
    this.play = new JButton(icon);
    this.play.addActionListener(this);
    this.play.setActionCommand("Pause/Play");
    JPanel playPanel = new JPanel(new BorderLayout(5, 5));
    playPanel.add(play, BorderLayout.EAST);
    this.add(playPanel, BorderLayout.NORTH);

    //Put the movements panel in the middle
    this.add(movementsPanel, BorderLayout.CENTER);
  }

  /**
   * Add a list of actions to a movement
   * @param movementID the id of the movement
   * @param actions the list of actions
   * @see MovementsPanel#addActions(org.musichands.musichands.movements.MovementID,
   * java.util.List)
   * @see List
   * @see Action
   */
  public void addActions (MovementID movementID, List<Action> actions) {
    movementsPanel.addActions(movementID, actions);
  }

  /**
   * Change the state of the program pause <-> play
   * @param state true for play - false for pause
   * @see Controller#pause_play()
   * @see MainInterface#enableComponents(java.awt.Container, boolean)
   */
  public void changeState (boolean state) {
    enableComponents(actionsSelectorPanel, state);
    enableComponents(movementsPanel, state);
  }

  /**
   * Recursive disabel/enable all the elements inside a component
   * @param container The container with the elements
   * @param enable true for enable, false for disable
   * This code was taken from: @Andrew Thompson
   * http://stackoverflow.com/questions/10985734/java-swing-enabling-disabling-all-components-in-jpanel
   */
  private void enableComponents(Container container, boolean enable) {
    Component[] components = container.getComponents();
    for (Component component : components) {
      component.setEnabled(enable);
      if (component instanceof Container) {
        enableComponents((Container)component, enable);
      }
    }
  }

  /**
   * Activate the no connection mode
   * Blocks the pause/play button
   * And show the user a warning message
   */
  public void noConnection () {
    this.play.setEnabled(false);
    JOptionPane.showMessageDialog(this,
                                  "<html><b>Couldn't establish a connection with"
                                  + " the glove, you can still use the program in"
                                  + " the configuration mode, but cannot activate"
                                  + " the reproduction mode<br>If you are planning"
                                  + " to use the reproduction mode please make"
                                  + " sure either the glove and the pc are"
                                  + " connected to the same wifi.</b></html>",
                                  "NO CONNECTION WITH THE GLOVE",
                                  JOptionPane.WARNING_MESSAGE);
  }

  /**
   * This method handle all the event that occur in this interface
   * @param e The event that occurred
   */
  @Override
  public void actionPerformed(ActionEvent e) {
    switch (e.getActionCommand()) {
    case "Select actions":
      //Start the selection of action for a movement
      actionsSelectorPanel.uncheckAll();
      this.remove(movementsPanel);
      this.add(actionsSelectorPanel, BorderLayout.CENTER);
      this.revalidate();
      this.repaint();
      break;
    case "Add actions":
      //Add the actions selected by the user in ActionsSelectorPanel
      //To the movement selected in the MovementsPanel
      List<Action> actions = actionsSelectorPanel.getSelectedActions();
      Controller.addActions(movementsPanel.getSelectedMovement(), actions);
      movementsPanel.addActions(actions);
    case "Cancel":
      //Cancel the selection of actions
      this.remove(actionsSelectorPanel);
      this.add(movementsPanel, BorderLayout.CENTER);
      this.revalidate();
      this.repaint();
      break;
    case "Pause/Play":
      //Change the state of the program
      Controller.pause_play();
      break;
    case "Remove all":
      //Remove all the actions of the actual selected movement
      movementsPanel.removeAllActions();
      this.revalidate();
      this.repaint();
      break;
    case "Add module":
      //Show a new Download Dialog
      new ModuleDialog(SwingUtilities.getWindowAncestor(this)).setVisible(true);
      break;
    case "Load":
      //Clear the actual configuration
      movementsPanel.clearAll();
      Controller.clear();
      //Load the profile selected by the user
      Controller.loadProfile(movementsPanel.getProfileName());
      //Reload the gui
      this.revalidate();
      this.repaint();
      break;
    case "Save":
      //Save the actual configuration
      Controller.saveProfile(movementsPanel.getProfileName());
      break;
    case "Delete":
      //Delete the profile
      Controller.deleteProfile(movementsPanel.getProfileName());
      movementsPanel.deleteSelectProfile();
      break;
    case "Profiles":
      //If the selected profile is "New"...
      String selectedProfile = movementsPanel.getProfileName();
      if (selectedProfile != null && selectedProfile.equals("New")) {
        //... show an input dialog for the user type the new profile name
        String profileName =
          JOptionPane.showInputDialog("Type the name of the new profile");
        //profileName is null, if the user click the "cancel" button
        if (profileName != null && !profileName.equals(""))
          movementsPanel.addProfile(profileName);
        else
          movementsPanel.setDefaultProfile();
      }
      break;
    }
  }
}
