package org.musichands.musichands.gui;

import java.awt.GridLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.musichands.musichands.backend.ModuleInstaller;

/**
 * Dialog for the user type the module purchase code and start its download
 * @author Luis Miguel Mejía Suárez
 * @see JDialog
 * @see ModuleInstaller
 * @see TextPrompt
 */
public class ModuleDialog extends JDialog implements ActionListener {
  //Text prompt for the user type the module purchase code
  private final JTextField moduleCodePrompt;

  //Label to show info to the user
  private final JLabel text;

  //Buttons to cancel or start the download
  private final JButton download;
  private final JButton cancel;

  /**
   * Constructor of the class, initialice all the buttons and TextPrompt
   * TextPrompt moduleCode, prompt to type the module code
   * JButton "Download", button to start donwload the module
   * JButton "Cancel", button to cancel this action an destroy this Dialog
   * @param parent The parent window for this dialog
   * @see JDialog#JDialog(java.awt.Window,
   * java.lang.String, java.awt.Dialog.ModalityType)
   * @see javax.swing.JDialog.ModalityType#APPLICATION_MODAL
   * @see TextPrompt
   * @see JButton
   */
  public ModuleDialog (Window parent) {
    //Call the constructor of my parent, set the tittle and modal true
    super(parent, "Download Module", JDialog.ModalityType.APPLICATION_MODAL);

    //Set the layout, size and close operation of this frame
    this.setLayout(new GridLayout(0, 1));
    this.setSize(250, 150);
    this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

    //Set the text to "Enter the module purchase code"
    text = new JLabel("Enter the module purchase code");
    this.add(text);

    //Create a new text prompt for the user type the module pruchase code
    moduleCodePrompt = new JTextField("");
    moduleCodePrompt.setEditable(true);
    new TextPrompt("xxxx-xxxx", moduleCodePrompt);
    this.add(moduleCodePrompt);

    //ButttonsPanel ----------------------------------------------------------
    JPanel panelButtons = new JPanel(new GridLayout(1, 2));

    //Cancel button
    cancel = new JButton("Cancel");
    cancel.setActionCommand("Cancel");
    cancel.addActionListener(this);
    panelButtons.add(cancel);

    //Download button
    download = new JButton("Download");
    download.setActionCommand("Download");
    download.addActionListener(this);
    panelButtons.add(download);

    //Set the download button as the default (Enter key)
    this.getRootPane().setDefaultButton(download);

    this.add(panelButtons);
    //------------------------------------------------------------------------
  }

  /**
   * Set the text to show to the user
   * @param text the text to show
   * @see JLabel#setText(java.lang.String)
   */
  public void setText (String text) {
    this.text.setText(text);
  }

  /**
   * Enable the text field and the buttons again
   * for the user type a new module purchase code
   */
  public void enableDialog () {
    this.cancel.setEnabled(true);
    this.download.setEnabled(true);
    this.moduleCodePrompt.setEnabled(true);
  }

  /**
   * This method handle all the event that occur in this interface
   * @param e The event that occurred
   */
  @Override
  public void actionPerformed(ActionEvent e) {
    //Check the command
    switch (e.getActionCommand()) {
    case "Download":
      //If the commmand is donwload, start the donwload of the module
      this.moduleCodePrompt.setEnabled(false);
      this.download.setEnabled(false);
      this.cancel.setEnabled(false);
      new Thread(new ModuleInstaller(moduleCodePrompt.getText(), this)).start();
      break;
    case "Cancel":
      //If the commmand is cancel, destroy this Dialog
      this.setVisible(false);
      this.dispose();
      break;
    default:
      //If the command is unkonw print an error
      System.err.println("Error: unknown command " + e.getActionCommand());
      break;
    }
  }
}
