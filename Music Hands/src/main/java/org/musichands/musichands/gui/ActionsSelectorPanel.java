package org.musichands.musichands.gui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import org.musichands.musichands.modules.Module;
import org.musichands.musichands.movements.actions.Action;
import org.musichands.musichands.movements.actions.ActionsFactory;
import org.musichands.musichands.movements.actions.ActivitiesFactory;

/**
 * This class is a JPanel to store all the information
 *  of all the installed modules 
 * @author Luis Miguel Mejía Suárez
 * @see JPanel
 * @see Module
 * @see ModulePanel
 */
public class ActionsSelectorPanel extends JPanel {
  //Each module will be a tab
  private final JTabbedPane modules;

  /**
   * Constructor of the class, creates an ModulePanel for each module installed
   * @param actionListener an ActionListener for the events in the JPanel
   * @see JPanel
   * @see ActionListener
   * @see ModulePanel
   * @see Module
   */
  protected ActionsSelectorPanel (ActionListener actionListener) {
    //Call the constructor of my parent (JPanel) with a new BorderLayout
    super(new BorderLayout(5, 5));

    //Add a new panel with the tittle of this panel in the top
    JPanel tittlePanel = new JPanel(new BorderLayout(5, 5));
    JLabel tittle = new JLabel("Select the actions you want to add",
                               SwingConstants.CENTER);
    tittle.setFont(new Font("Serif", Font.BOLD, 20));
    tittlePanel.add(tittle, BorderLayout.CENTER);
    this.add(tittlePanel, BorderLayout.NORTH);

    //ButtonsPanel -------------------------------------------------------------
    //Create a new panel for buttons
    JPanel buttonsPanel = new JPanel(new GridBagLayout());
    final GridBagConstraints c = new GridBagConstraints();
    c.gridy = 0;
    c.weightx = 1.0;
    c.weighty = 1.0;
    c.gridheight = 1;
    c.fill = GridBagConstraints.HORIZONTAL;

    //Add to buttonsPanel a new button "Add new module" on the left side
    c.gridx = 0;
    c.gridwidth = 1;
    c.anchor = GridBagConstraints.SOUTHWEST;
    JButton addModule = new JButton("Add new module");
    addModule.setActionCommand("Add module");
    addModule.addActionListener(actionListener);
    buttonsPanel.add(addModule, c);

    //Add to buttonsPanel a new empty label to fill the middle space
    c.gridx = 1;
    c.gridwidth = 4;
    c.anchor = GridBagConstraints.SOUTH;
    buttonsPanel.add(new JLabel(), c);

    //Add to buttonsPanel a new button "Cancel" in the rigth side
    c.gridx = 5;
    c.gridwidth = 1;
    c.anchor = GridBagConstraints.SOUTHEAST;
    JButton cancel = new JButton("Cancel");
    cancel.setActionCommand("Cancel");
    cancel.addActionListener(actionListener);
    buttonsPanel.add(cancel, c);

    //Add to buttonsPanel a new button "Add actions" next to "Cancel"
    c.gridx = 6;
    c.gridwidth = 1;
    c.anchor = GridBagConstraints.SOUTHEAST;
    JButton addActions = new JButton("Add actions");
    addActions.setActionCommand("Add actions");
    addActions.addActionListener(actionListener);
    buttonsPanel.add(addActions, c);

    //Add buttonsPanel to this in the bottom
    this.add(buttonsPanel, BorderLayout.SOUTH);
    //--------------------------------------------------------------------------

    //Modules ------------------------------------------------------------------
    //Create a new tab for every module installed
    this.modules = new JTabbedPane(JTabbedPane.TOP,
                                   JTabbedPane.SCROLL_TAB_LAYOUT);

    //Use an instance of ActivitiesFactory as the deafult ActionsFactory for
    //every ModulePanel
    ActionsFactory actionsFactory = new ActivitiesFactory();

    //Try to dinamically load every installed module
    try {
      //Open the file with the name of every installed module
      //The file is inside the jar, and every line is the name of a module
      Scanner moduleFile =
        new Scanner(this.getClass().getResourceAsStream("/resources/modules"));

      //While the file has more modules...
      Class moduleClass;
      Module<?, String> module;
      while (moduleFile.hasNext()) {
        //... load the class of the module
        moduleClass = Class.forName("org.musichands.musichands.modules."
                                    + moduleFile.nextLine());

        //create an instance of the module
        module = (Module<?, String>) moduleClass.newInstance();

        //create a tab (ModulePanel) with the module
        modules.addTab(module.getName(), module.getIcon(),
                       new ModulePanel(module, actionsFactory));
      }
    } catch (ClassNotFoundException | IllegalAccessException |
             InstantiationException ex) {
      System.err.println("Erro while loading modules\n"
                         + ex.getLocalizedMessage());
    }

    //Add to this the taps of modules in the center of the panel
    this.add(modules, BorderLayout.CENTER);
    //--------------------------------------------------------------------------
  }

  /**
   * @return all the actions in all modules that were selected by the user
   * @see ModulePanel#getSelectedActions()
   * @see List
   * @see Action
   */
  public List<Action> getSelectedActions () {
    List<Action> selectedActions = new ArrayList<>();

    //add all the selected actions of every module
    int tapsCount = modules.getTabCount();
    ModulePanel module;
    for (int i = 0; i < tapsCount; i++) {
      module = (ModulePanel) modules.getComponentAt(i);
      selectedActions.addAll(module.getSelectedActions());
    }

    return selectedActions;
  }

  /**
   * Uncheck all the actions of every module
   * @see ModulePanel#uncheckAll() 
   */
  public void uncheckAll () {
    int tapsCount = modules.getTabCount();
    ModulePanel module;
    for (int i = 0; i < tapsCount; i++) {
      module = (ModulePanel) modules.getComponentAt(i);
      module.uncheckAll();
    }
  }
}
