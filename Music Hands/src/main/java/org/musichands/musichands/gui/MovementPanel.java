package org.musichands.musichands.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import org.musichands.musichands.backend.Controller;
import org.musichands.musichands.movements.MovementID;
import org.musichands.musichands.movements.actions.Action;

/**
 * This class is a JPanel to show all the information of a Movement
 * @author Johan Sebastián Yepes Rios
 * @see JPanel
 * @see MovementID
 */
public class MovementPanel extends JPanel {
  //The ID of the real movement of this panel
  private final MovementID movement;

  //All the actions associated to this movement
  private final JPanel movementActions;

  /**
   * Constructor of the class, receives the movement to represent
   * @param movement the id of the movement
   * @param actionListener an ActionListener for the events of this interface
   * @see MovementID
   * @see ActionListener
   */
  protected MovementPanel (MovementID movement, ActionListener actionListener) {
    //Call the constructor of my parent (JPanel) with a new BorderLayout
    super(new BorderLayout(5, 5));

    //Panel for the movement image and description
    JPanel data = new JPanel(new BorderLayout(5, 5));
    JLabel tittle = new JLabel("Edit the actions for this movement");
    tittle.setFont(new Font("Serif", Font.ITALIC, 15));
    data.add(tittle, BorderLayout.NORTH);
    ImageIcon icon = new ImageIcon(movement.getMovementImage().getImage()
                                   .getScaledInstance(175, 150,
                                                      Image.SCALE_SMOOTH));
    data.add(new JLabel(icon), BorderLayout.WEST);
    data.add(new JLabel(movement.getDescription()), BorderLayout.EAST);
    data.setBorder(BorderFactory.createTitledBorder(movement.name()));
    this.add(data, BorderLayout.NORTH);

    //Creates a JPanel to add every action of this movement
    //Use a BoxLayout to maintain the maximum size of every MovementActionPanel
    //Use a JScrollPanel in case they are too many for the screen size
    this.movement = movement;
    this.movementActions = new JPanel();
    this.movementActions.setLayout(new BoxLayout(movementActions,
                                                 BoxLayout.Y_AXIS));
    this.add(new JScrollPane(movementActions), BorderLayout.CENTER);

    //ButttonsPanel ------------------------------------------------------------
    //Create a new panel for buttons
    JPanel buttonsPanel = new JPanel(new BorderLayout(5, 5));

    //Add to buttonsPanel a new button "Select Actions" on the rigth side
    JButton select = new JButton("Select Actions");
    select.setActionCommand("Select actions");
    select.addActionListener(actionListener);
    select.setForeground(Color.GREEN);
    buttonsPanel.add(select, BorderLayout.EAST);

    //Add to buttonsPanel a new button "Remove all Actions" on the left side
    JButton remove = new JButton("Remove all Actions");
    remove.setActionCommand("Remove all");
    remove.addActionListener(actionListener);
    remove.setForeground(Color.RED);
    buttonsPanel.add(remove, BorderLayout.WEST);

    //Add buttonsPanel to this in the bottom
    this.add(buttonsPanel, BorderLayout.SOUTH);
    //--------------------------------------------------------------------------
  }

  /**
   * @return the MovementID of this panel
   * @see MovementID
   */
  public MovementID getMovement () {
    return this.movement;
  }

  /**
   * Remove all actions associated to this movement
   * @see Controller#removeAction(org.musichands.musichands.movements.MovementID
   * ,org.musichands.musichands.movements.actions.Action)
   * @see JPanel#removeAll()
   */
  public void removeAllActions () {
    MovementActionPanel movementAction;
    //For every component in movementActions...
    for (Component comp : movementActions.getComponents()) {
      //... if the component is a MovementActionPanel ...
      if (comp instanceof MovementActionPanel) {
        //... remove that action of this movement
        movementAction = (MovementActionPanel) comp;
        Controller.removeAction(movement, movementAction.action);
      }
    }

    //Clear movementActions
    this.movementActions.removeAll();
  }

  /**
   * Add a new
   * {@link org.musichands.musichands.gui.MovementPanel.MovementActionPanel}
   * for every action
   * added to this movement
   * @param actions the list of actions to add
   * @see List
   * @see Action
   * @see JPanel#add(java.awt.Component)
   */
  public void addActions (List<Action> actions) {
    //For every Action in actions ...
    MovementActionPanel movementActionPanel;
    for (Action action : actions) {
      //add a new MovementActionPanel
      movementActionPanel = new MovementActionPanel(this, action);
      movementActions.add(movementActionPanel);

      //and add a gap between them
      movementActions.add(movementActionPanel.gap);
    }
  }

  /**
   * This class is a JPanel to show the actions of a movement
   * @author Johan Sebastián Yepes Rios
   * @see JPanel
   * @see ActionListener
   */
  private class MovementActionPanel extends JPanel implements ActionListener {
    //The movement to which this action belongs
    private final MovementPanel parent;

    //The real action of this representation
    private final Action action;

    //A gap to separate this JPanel from others
    private final Component gap;

    /**
     * Constructor of the class, can only be called from MovementPanel
     * @param parent the movement to which this action belongs
     * @param action the real action of this representation
     * @see MovementPanel
     * @see Action
     */
    private MovementActionPanel (MovementPanel parent, Action action) {
      //Call the constructor of my parent (JPanel) with a new BorderLayout
      super(new BorderLayout(5, 5));

      //Save the variables received in the constructor
      this.parent = parent;
      this.action = action;

      //Initialize a new rigid area of any horizontal size in 5 in vertical
      this.gap = Box.createRigidArea(new Dimension(0, 5));

      //Set the size of this panel
      //we don´t care of the horizontal size so it can be any
      //and 35 in the vertical size
      this.setPreferredSize(new Dimension(0, 35));
      this.setMaximumSize(new Dimension(Integer.MAX_VALUE, 35));

      //Set a black border for this
      this.setBorder( BorderFactory.createLineBorder(Color.BLACK));

      //Show the name of the action in the left side
      this.add(new JLabel(action.getName()), BorderLayout.WEST);

      //Show a "X" (Delete) button in the rigth side
      JButton delete = new JButton("X");
      delete.setActionCommand("Delete");
      delete.addActionListener(this);
      delete.setForeground(Color.RED);
      this.add(delete, BorderLayout.EAST);
    }

    /**
     * This method handle all the event that occur in this interface
     * @param e The event that occurred
     */
    @Override
    public void actionPerformed(ActionEvent e) {
      switch (e.getActionCommand()) {
      case "Delete":
        //Graphically remove this action from the MovementPanel which it belongs
        parent.movementActions.remove(this);
        parent.movementActions.remove(this.gap);

        //Reload (repaint) the MovementPanel
        parent.revalidate();
        parent.repaint();

        //Logically remove this action from the Movement which it belongs
        Controller.removeAction(parent.getMovement(), action);
        break;
      }
    }
  }
}
