package org.musichands.musichands.gui;

import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * This frame is used to show images
 * This class is a singleton
 * @author Alejandro Salgado Gómez
 * @see JFrame
 * @see ImageIcon
 */
public class Screen extends JFrame {
  //The unique instance of this class
  private static volatile Screen INSTANCE = null;

  /**
   * @return The instance of this class
   */
  public static synchronized Screen getInstance () {
    if (INSTANCE == null) INSTANCE = new Screen();
    return INSTANCE;
  }

  //The label where the image will go
  private final JLabel imageLabel;

  /**
   * Constructor of the class
   * Called only once in the application start up
   * @see JFrame#JFrame(java.lang.String)
   */
  private Screen () {
    //Call the constructor of my parent (JFrame) with the tittle
    super("SCREEN");
    
    //Set the size, layout and close operation
    this.setSize(500, 500);
    this.setLayout(new BorderLayout());
    this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

    //Add a new Label for the images
    this.imageLabel = new JLabel(new ImageIcon());
    this.add(imageLabel, BorderLayout.CENTER);

    //Add to the label a Component Adapter to resize the size of the image
    //when the frame is resized
    imageLabel.addComponentListener(new ComponentAdapter() {
        @Override
        public void componentResized(ComponentEvent e) {
          ImageIcon var = (ImageIcon) imageLabel.getIcon();
          imageLabel.setIcon
            (new ImageIcon(var.getImage().getScaledInstance
                           (imageLabel.getBounds().width,
                            imageLabel.getBounds().width,
                            Image.SCALE_REPLICATE)));
        }
      });
  }

  /**
   * This method is called from ScreenEffects to change the image of the label
   * @return the label to show images
   * @see JLabel
   * @see JLabel#setIcon(javax.swing.Icon)
   * @see org.musichands.musichands.movements.actions.ScreenEffects
   */
  public JLabel getImageLabel () {
    return this.imageLabel;
  }
}
