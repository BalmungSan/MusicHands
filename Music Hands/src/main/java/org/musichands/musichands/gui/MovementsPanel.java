package org.musichands.musichands.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import org.musichands.musichands.movements.MovementID;
import org.musichands.musichands.movements.actions.Action;

/**
 * This class is a JPanel to show the information of the actual configuration
 * @author Johan Sebastián Yepes Rios
 * @see JPanel
 * @see MovementPanel
 */
public class MovementsPanel extends JPanel {
  //Each movement will be a tab
  private final JTabbedPane movements;

  //A combobox for the user profiles
  private final JComboBox<String> profiles;

  /**
   * Constructor of the class
   * initialize every movement in MovementID (Enum) as a new tab (MovementPanel)
   * this panel uses a BorderLayout
   * @param actionListener an ActionListener for the events in the JPanel
   * @param names all the user profiles names
   * @see MovementID
   * @see MovementPanel
   * @see ActionListener
   * @see BorderLayout
   * @see JPanel#JPanel(java.awt.LayoutManager)
   */
  protected MovementsPanel (ActionListener actionListener, List<String> names) {
    //Call the constructor of my parent (JPanel) with a new BorderLayout
    super(new BorderLayout(5, 5));

    //Create a JPanel for the tittle of this panel, goes in the top part
    JPanel tittlePanel = new JPanel(new BorderLayout(5, 5));
    JLabel tittle = new JLabel("This is your current configuration",
                               SwingConstants.CENTER);
    tittle.setFont(new Font("Serif", Font.BOLD, 20));
    tittlePanel.add(tittle, BorderLayout.CENTER);
    this.add(tittlePanel, BorderLayout.NORTH);

    //ButtonsPanel -------------------------------------------------------------
    //Create a new panel for buttons
    JPanel buttonsPanel = new JPanel(new GridBagLayout());
    final GridBagConstraints c = new GridBagConstraints();
    c.gridy = 0;
    c.weightx = 1.0;
    c.weighty = 1.0;
    c.gridheight = 1;
    c.fill = GridBagConstraints.HORIZONTAL;

    //Add a "Profile" label in the left side
    c.gridx = 0;
    c.gridwidth = 2;
    c.anchor = GridBagConstraints.SOUTHWEST;
    buttonsPanel.add(new JLabel("Profile"), c);

    //Add a Combo box for select the profile in the middle
    c.gridx = 2;
    c.gridwidth = 2;
    c.anchor = GridBagConstraints.SOUTH;
    profiles = new JComboBox(new String[] {"New"});
    for (String profile : names) profiles.addItem(profile);
    profiles.setSelectedIndex(-1);
    profiles.addActionListener(actionListener);
    profiles.setActionCommand("Profiles");
    profiles.setEditable(false);
    buttonsPanel.add(profiles, c);

    //Add a "Load" button  in the rigth side
    c.gridx = 4;
    c.gridwidth = 1;
    c.anchor = GridBagConstraints.SOUTHEAST;
    JButton load = new JButton("Load");
    load.setActionCommand("Load");
    load.addActionListener(actionListener);
    buttonsPanel.add(load, c);

    //Add a "Save" button next to "Load"
    c.gridx = 5;
    c.gridwidth = 1;
    c.anchor = GridBagConstraints.SOUTHEAST;
    JButton save = new JButton("Save");
    save.setActionCommand("Save");
    save.addActionListener(actionListener);
    buttonsPanel.add(save, c);

    //Add a "Delete" button next to "Save"
    c.gridx = 6;
    c.gridwidth = 1;
    c.anchor = GridBagConstraints.SOUTHEAST;
    JButton delete = new JButton("Delete");
    delete.setActionCommand("Delete");
    delete.addActionListener(actionListener);
    delete.setForeground(Color.RED);
    buttonsPanel.add(delete, c);

    //Add buttonsPanel to this in the bottom
    this.add(buttonsPanel, BorderLayout.SOUTH);
    //--------------------------------------------------------------------------

    //Movements ----------------------------------------------------------------
    //Create a new tab for every movement in MovementID (Enum)
    this.movements = new JTabbedPane(JTabbedPane.LEFT,
                                     JTabbedPane.SCROLL_TAB_LAYOUT);

    //For each MovementID ...
    MovementPanel movementPanel;
    for (MovementID movement : MovementID.values()) {
      //... create a new MovementPanel
      movementPanel = new MovementPanel(movement, actionListener);

      //add the MovementPanel as a new tab
      movements.addTab(movement.name(), movementPanel);
    }

    //Add to this the taps of movements in the center of the panel
    this.add(movements, BorderLayout.CENTER);
    //--------------------------------------------------------------------------
  }

  /**
   * Add a list of actions to the selected movement (tab)
   * @param actions the list of actions
   * @see JTabbedPane#getSelectedComponent()
   * @see MovementPanel#addActions(java.util.List)
   * @see List
   * @see Action
   */
  public void addActions (List<Action> actions) {
    MovementPanel movementSelectedPanel =
      (MovementPanel) movements.getSelectedComponent();
    movementSelectedPanel.addActions(actions);
  }

  /**
   * Add a list of actions to a movement (tab)
   * @param movementID the id of the movement
   * @param actions the list of actions
   * @see JTabbedPane#getComponentAt(int)
   * @see MovementPanel#addActions(java.util.List)
   * @see List
   * @see Action
   */
  public void addActions (MovementID movementID, List<Action> actions) {
    MovementPanel movementSelectedPanel =
      (MovementPanel) movements.getComponentAt(movementID.getID());
    movementSelectedPanel.addActions(actions);
  }

  /**
   * @return the selected movement (tab)
   * @see JTabbedPane#getSelectedComponent()
   * @see MovementPanel#getMovement()
   * @see MovementID
   */
  public MovementID getSelectedMovement () {
    MovementPanel movementSelectedPanel =
      (MovementPanel) movements.getSelectedComponent();
    return movementSelectedPanel.getMovement();
  }

  /**
   * Remove all the actions of the selected movement
   * @see JTabbedPane#getSelectedComponent()
   * @see MovementPanel#removeAllActions()
   */
  public void removeAllActions () {
    MovementPanel movementSelectedPanel =
      (MovementPanel) movements.getSelectedComponent();
    movementSelectedPanel.removeAllActions();
  }

  /**
   * Remove all the actions of every movement
   * @see JTabbedPane#getComponents()
   * @see MovementPanel#removeAllActions()
   */
  public void clearAll () {
    for (Component movement : movements.getComponents()) {
      if (movement instanceof MovementPanel)
        ((MovementPanel) movement).removeAllActions();
    }
  }

  /**
   * @return the name of the actual selected profile
   */
  public String getProfileName () {
    return profiles.getItemAt(profiles.getSelectedIndex());
  }

  /**
   * Add a new profile name to the combo box of profiles
   * @param profileName the new profile name
   * @see #profiles
   * @see JComboBox#addItem(java.lang.Object)
   */
  public void addProfile (String profileName) {
    this.profiles.addItem(profileName);
    this.profiles.setSelectedIndex(profiles.getItemCount() - 1);
  }

  /**
   * Delete the actual selected profile
   */
  public void deleteSelectProfile () {
    this.profiles.removeItem(getProfileName());
    setDefaultProfile();
  }

  /**
   * Set the deafult profile, empty (-1)
   * @see #profiles
   * @see JComboBox#setSelectedIndex(int)
   */
  public void setDefaultProfile() {
    this.profiles.setSelectedIndex(-1);
  }
}
