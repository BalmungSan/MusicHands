package org.musichands.musichands.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.musichands.musichands.backend.Controller;
import org.musichands.musichands.movements.actions.Action;
import org.musichands.musichands.movements.actions.ImagesFactory;

/**
 * This class is a JPanel to contain all the information of an action
 * @author Luis Miguel Mejía Suárez
 * @see Action
 * @see JPanel
 * @see GridBagLayout
 */
public class ActionPanel extends JPanel implements ActionListener {
  //Static GribBagConstrains to manage the layout of the elements of this class
  //Is static to have only one istnace live in all the application life time
  private static final GridBagConstraints C = new GridBagConstraints();

  //A checkboc for the user select or not this action
  private final JCheckBox select;

  //The tittle or name of this action
  protected final JLabel title;

  //The real action of this panel
  protected Action action;

  /**
   * Private constructor of this class, can only be accesed by the builder
   * Initialize a default action panel with an action.
   * @param action The action of this panel
   * @see Action
   * @see ActionPanelBuilder
   */
  private ActionPanel(Action action) {
    //Call the constructor of my parent (JPanel) with a new GridBagLayout
    super(new GridBagLayout());

    this.action = action;
    
    //Initialice the deafult values for C
    C.gridy = 0;
    C.weightx = 1.0;
    C.weighty = 1.0;
    C.gridheight = 1;
    C.fill = GridBagConstraints.BOTH;

    //Set the tittle of this as the name of the action
    this.title = new JLabel(action.getName());
    C.gridx = 0;
    C.gridwidth = 5;
    C.anchor = GridBagConstraints.NORTHEAST;
    this.add(title, C);

    //Add the checkbox to select or not this action
    this.select = new JCheckBox("Select");
    C.gridx = 9;
    C.gridwidth = 1;
    C.anchor = GridBagConstraints.NORTHWEST;
    this.add(select, C);
  }

  /**
   * @return The Action of this Panel
   * @see Action
   */
  public Action getAction () {
    return this.action;
  }

  /**
   * check if this action is selected
   * @return true if the check box is selected if not false
   * @see JCheckBox#isSelected()
   */
  public boolean isSelected () {
    return this.select.isSelected();
  }

  /**
   * Uncheck this check box
   * @see JCheckBox#setSelected(boolean)
   */
  public void uncheck () {
    this.select.setSelected(false);
  }

  /**
   * This method handle all the event that occur in this panel
   * @param e The event that occurred
   */
  @Override
  public void actionPerformed(ActionEvent e) {
    switch (e.getActionCommand()) {
    case "Play sound": case "Show image":
      //Set the prgram in play mode
      Controller.pause_play();
      
      Thread t = new Thread(action);

      //Execute this action an wait until it finish
      synchronized (t) {
        try {
          t.start();
          t.wait();
        } catch (InterruptedException ex) {
          System.out.println("Error executing the action: " + action.getName());
          System.err.println(ex.getLocalizedMessage());
        }
      }
      
      //Set the program to pause mode
      Controller.pause_play();
      break;
    }
  }

  /**
   * This class is the entry point to create an ActionPanel
   * This class is a builder
   * @author Luis Miguel Mejía Suárez
   * @see ActionPanel
   */
  public static class ActionPanelBuilder {
    /**
     * Creates an ActionPanel just with the name an the checkbox
     * Used for actions that are events like pause/play and change the volume
     * @param action the action for the ActionPanel 
     * @return an ActionPanel for events
     * @see Action
     * @see ActionPanel
     */
    protected static ActionPanel createEventPanel (Action action) {
      //Create a new ActionPanel
      ActionPanel eventPanel = new ActionPanel(action);

      //Add an empty JLabel in the center of it to fill the space
      C.gridx = 5;
      C.gridwidth = 4;
      C.anchor = GridBagConstraints.CENTER;
      eventPanel.add(new JLabel(), C);

      return eventPanel;
    }

    /**
     * Creates an action panel with a "Play" button in the middle
     * Used for actions that are sounds
     * @param action The action of the ActionPanel
     * @return an ActionPanel for sounds
     * @see Action
     * @see ActionPanel
     */
    protected static ActionPanel createSoundPanel (Action action) {
      //Create a new ActionPanel
      ActionPanel soundPanel = new ActionPanel(action);

      //Add "Play" Button for playing sounds in the middle of it
      C.gridx = 5;
      C.gridwidth = 4;
      C.anchor = GridBagConstraints.CENTER;
      JButton play = new JButton("Play");
      play.setActionCommand("Play sound");
      play.addActionListener(soundPanel);
      soundPanel.add(play, C);

      return soundPanel;
    }


    /**
     * Creates an ActionPanel with two buttons ("Show Image", "Select Image")
     * Used for actions that are images
     * @param action the action for the ActionPanel
     * @return an ActionPanel  for images
     * @see Action
     * @see ActionPanel
     */
    protected static ActionPanel createImagePanel (Action action) {
      /**
       * This class is an ActionPanel for images
       * @author Luis Miguel Mejia Suárez
       * @see ActionPanel
       */
      class ImagePanel extends ActionPanel {
        //File chooser to select the images
        private final JFileChooser chooser;
        private final FileNameExtensionFilter filter;

        /**
         * Constructor of the class
         * @param action The action of the ActionPanel
         * @see Action
         * @see ActionPanel
         */
        private ImagePanel (Action action) {
          //Call the constructor of ActionPanel
          super(action);

          //Initialice the file chooser an the extension filter
          this.chooser = new JFileChooser();
          this .filter = new FileNameExtensionFilter("Images",
                                                     "jpg", "png", "gif");
          chooser.setFileFilter(filter);
          chooser.setDialogTitle("Select a picture");
        }

        /**
         * This method handle all the event that occur in this panel
         * @param e The event that occurred
         */
        @Override
        public void actionPerformed(ActionEvent e) {
          switch (e.getActionCommand()) {
          case "Select image":
            //Create a file chooser for the user select the image
            int returnVal = chooser.showOpenDialog(this);

            //If an image was sucefully selected...
            if(returnVal == JFileChooser.APPROVE_OPTION) {
              //... change the image of this action with it
              String imagePath = chooser.getSelectedFile().getAbsolutePath();
              action = new ImagesFactory().createAction(imagePath);

              //and change the tittle of this ActionPanel
              this.title.setText(action.getName());
            }

            break;
          default:
            //Call my father if I can't handle this ActionEvent
            super.actionPerformed(e);
          }
        }
      }

      //Create a new image panel
      ActionPanel imagePanel = new ImagePanel(action);

      //Add a "Show Image" button, in the middle of it, to show the image 
      C.gridx = 5;
      C.gridwidth = 2;
      C.anchor = GridBagConstraints.CENTER;
      JButton show = new JButton("Show image");
      show.setActionCommand("Show image");
      show.addActionListener(imagePanel);
      imagePanel.add(show, C);

      //Add a "Select Image" button, in the middle of it, to select the image
      C.gridx = 7;
      C.gridwidth = 2;
      C.anchor = GridBagConstraints.CENTER;
      JButton select = new JButton("Select image");
      select.setActionCommand("Select image");
      select.addActionListener(imagePanel);
      imagePanel.add(select, C);

      return imagePanel;
    }
  }
}
