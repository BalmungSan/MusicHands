package org.musichands.musichands.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.Border;
import org.musichands.musichands.modules.Module;
import org.musichands.musichands.movements.actions.Action;
import org.musichands.musichands.movements.actions.ActionsFactory;

/**
 * This class is a JPanel to contain all the information of a module
 * @author Luis Miguel Mejía Suárez
 * @see JPanel
 * @see Module
 * @see ActionPanel
 */
public class ModulePanel extends JPanel {
  //All the actions of this module
  private final List<ActionPanel> actions;

  /**
   * Constructor of the class, receives:
   *   A Module with all the information to show here
   *   An ActionsFactory to create the Actions of the module
   * @param <M> Any kind of Module wich its output type is String
   * @param module The module with the data to show here
   * @param factory The factory to create the actions
   * @see Module
   * @see ActionsFactory
   */
  protected <M extends Module<?, String>> ModulePanel
    (M module, ActionsFactory factory) {
    //Call the constructor of my parent (JPanel) with a new BorderLayout
    super(new BorderLayout(5, 5));

    //Array list because better performance in get() operation
    this.actions = new ArrayList<>();

    //The description of the module
    JPanel description = new JPanel();
    description.add(new JLabel(module.getDescription()));
    description.setBorder(BorderFactory.createTitledBorder("Description"));
    this.add(description, BorderLayout.NORTH);

    //Use a BoxLayout to maintain the maximum size of every ActionPanel
    JPanel actionsPanel = new JPanel();
    actionsPanel.setLayout(new BoxLayout(actionsPanel, BoxLayout.Y_AXIS));

    //A black border for every ActionPanel
    Border border = BorderFactory.createLineBorder(Color.BLACK);

    //For every Action in the module...
    ActionPanel ap;
    Action action;
    for (String data : module.getAllData()) {
      action = factory.createAction(data);

      //... creates the specific ActionPanel for that type of action
      switch (data) {
      case "Pause/Play": case "Volume up": case "Volume down":
        ap = ActionPanel.ActionPanelBuilder.createEventPanel(action);
        break;
      case "Image":
        ap = ActionPanel.ActionPanelBuilder.createImagePanel(action);
        break;
      default:
        ap = ActionPanel.ActionPanelBuilder.createSoundPanel(action);
        break;
      }

      //set the border of the ActionPanel
      ap.setBorder(border);

      //set the size of the panel
      //we don´t care of the horizontal size so it can be any
      //and 30 in the vertical size
      ap.setPreferredSize(new Dimension(0, 30));
      ap.setMaximumSize(new Dimension(Integer.MAX_VALUE, 30));

      //add the ActionPanel and an empty space to separate the next
      this.actions.add(ap);
      actionsPanel.add(ap);
      actionsPanel.add(Box.createRigidArea(new Dimension(0, 5)));
    }

    //add all the actions in a scrollable panel in case they are too many
    this.add(new JScrollPane(actionsPanel), BorderLayout.CENTER);
  }

  /**
   * Iterate over all the ActionsPanel of this module and see if is selected
   * if true, get the Action of it
   * @return a List with all the selected actions in this module
   * @see ActionPanel#isSelected()
   * @see ActionPanel#getAction()
   * @see List
   * @see Action
   */
  public List<Action> getSelectedActions () {
    List<Action> selectedActions = new ArrayList<>();

    for (ActionPanel ap : actions) {
      if (ap.isSelected()) selectedActions.add(ap.getAction());
    }

    return selectedActions;
  }

  /**
   * Un check every ActionPanel in this module
   * @see ActionPanel#uncheck()
   */
  public void uncheckAll () {
    for (ActionPanel ap : actions) {
      ap.uncheck();
    }
  }
}
