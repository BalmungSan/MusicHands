package org.musichands.musichands.modules;

import java.awt.Image;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 * This class is the template for every module
 * @param <E> The type of the enum of the module
 * @param <S> The type of the output of the module
 * @author Luis Miguel Mejía Suárez
 */
public abstract class Module <E extends Enum<E>, S> {
  //The class of the enum
  private final Class<E> moduleEnum;

  //The date of the module
  private final String name;
  private final String description;
  private final Icon icon;


  /**
   * Constructor the module, receives all the data of the module
   * @param moduleEnum the class of the enum
   * @param name the name of the module
   * @param description the description of the module
   * @param icon the path to the icon of the module
   */
  protected Module (Class<E> moduleEnum, String name,
                    String icon, String description) {
    this.moduleEnum = moduleEnum;
    this.name = name;
    this.description = description;
    this.icon = new ImageIcon(new ImageIcon(this.getClass().getResource(icon))
                              .getImage().getScaledInstance(35, 35,
                                                            Image.SCALE_SMOOTH));
  }

  /**
   * @return a list with all the data of the module
   * @see List
   * @see Enum#toString()
   */
  public List<S> getAllData() {
    List<S> allData = new ArrayList<>();

    //For each element in the enum ...
    for (Enum<E> element : moduleEnum.getEnumConstants())
      //... cast the data of the enum to <S>
      allData.add((S) element.toString());

    return allData;
  }

  /**
   * @return the name of the module
   */
  public String getName (){
    return this.name;
  }

  /**
   * @return the icon of the module
   * @see ImageIcon
   */
  public Icon getIcon () {
    return this.icon;
  }

  /**
   * @return the description of the module
   */
  public String getDescription () {
    return this.description;
  }
}
