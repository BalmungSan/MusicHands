package org.musichands.musichands.modules;

/**
 * This is the module for activties such as
 * pause/play, show images and chage the volume
 * @author Luis Miguel Mejía Suárez
 * @see Module
 */
public class ModuleActivities extends Module<ActivitiesEnum, String> {
  /**
   * Constructor, call the constructor of the module
   * @see Module#Module(java.lang.Class, java.lang.String,
   * java.lang.String, java.lang.String) 
   */
  public ModuleActivities () {
    super(ActivitiesEnum.class,
          "Activities",
          "/resources/images/activities.gif",
          "<html><b>Este módulo contiene varias acciones que pueden "
          + "ser realizadas con el guante.</b>"
          + "<br></br>"
          + "Tales como:"
          + "<br></br>"
          + "<ul><li>pausar/reanudar el modo reproducción.</li>"
          + "<li>Subir o bajar el volumen.</li>"
          + "<li>Y presentar imagenes en pantalla</li></ul></html>");
  }
}

/**
 * This enum contains all the actions of this module
 * @author Luis Miguel Mejía Suárez
 */
enum ActivitiesEnum {
  //Enum constants
  PAUSE_PLAY("Pause/Play"),
  VOLUME_UP("Volume up"),
  VOLUME_DOWN("Volume down"),
  IMAGE("Image");

  //The data of the element of the enum
  private final String data;

  /**
   * Constructor of the enum
   * Can't be called
   * @param data the data of the element
   */
  private ActivitiesEnum(String data) {this.data = data;};

  /**
   * @return the data of the element
   */
  @Override
  public String toString () {
    return this.data;
  }
}
