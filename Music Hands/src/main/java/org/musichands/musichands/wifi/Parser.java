package org.musichands.musichands.wifi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Set;
import javax.imageio.IIOException;
import org.musichands.musichands.backend.Controller;
import org.musichands.musichands.movements.Movement;
import org.musichands.musichands.movements.MovementID;
import org.musichands.musichands.movements.actions.Action;

/**
 * This class is in charge of analyze the data read from wifi
 * This class is a singleton
 * @author Luis Miguel Mejía Suárez
 */
public class Parser implements Runnable {
  //The unique instance of this class
  private static volatile Parser INSTANCE = null;

  /**
   * @return The instance of this class
   */
  public static synchronized Parser getInstance () {
    try {
      //If is the first time this method is called, create the instance
      if (INSTANCE == null) INSTANCE = new Parser();
      Controller.connection();
    } catch (IOException ex) {
      //Show an error and stop the execution if there is a problem
      //with the socket with the arduino
      System.err.println("ERROR: " + ex.getMessage());
      Controller.noConnection();
    }

    return INSTANCE;
  }

  //The Wifi reader
  private final Reader reader;

  //The set with the id of valid movements
  private final Set<MovementID> filter;

  //Flag to determine if the execution of this thread should continue
  private boolean keepRunning;

  /**
   * Private constructor of the class
   * Initialize tthe reader and filter
   * @throws IOException If the reader cannot create the socket
   */
  private Parser () throws IOException {
    this.keepRunning = true;
    this.reader = new Reader();
    this.filter = Controller.getValidMovements();
  }

  /**
   * This method analyzes the data read by reader
   */
  @Override
  public void run () {
    //Create a new thread for the reader
    new Thread(reader).start();

    //Runs forever since the start of the program until it's shutdown
    int data;
    MovementID movementID;
    while (keepRunning) {

      //If the reader has a new signal ...
      if (reader.isAvailable()) {
        data = reader.nextInt(); //retrieve it and
        movementID = MovementID.getMovement(data); //get the MovementID of it

        //If the id is in the filter
        if (filter.contains(movementID))
          //Get the movement associated with that id and send it to player
          startActions(Controller.getMovement(movementID));
      }
    }
  }

  /**
   * Start all the actions of a movement in different threads
   * @param movement The movement
   * @see Movement#getActios()
   * @see Action
   * @see Thread#start()
   */
  public static void startActions (Movement movement) {
    for (Action action : movement.getActios()) {
      new Thread(action).start();
    }
  }

  /**
   * Stop the execution of this thread and the thread of reader
   */
  public void stop () {
    this.keepRunning = false;
    reader.stop();
  }


  /**
   * This class is in charge of read the data the glove(s) send by wifi
   * @author Johan Sebastian Yepes Rios
   */
  class Reader implements Runnable {
    //The last data read
    private int data;

    //Flag to indicate when new data are ready
    private boolean available;

    //The socket with the arduino (glove)
    private final Socket arduinoSocket;

    //The stream to read the data sent by the socket
    private final BufferedReader in;

    //The stream to sent data to the socket
    private final PrintWriter out;

    /**
     * The constructor initialize the socket to read
     * @throws IOException If cannot create the socket with the glove
     */
    public Reader () throws IOException {
      Socket aux;

      //Try to connect to to the arduino.local dns
      try {
        aux = new Socket();
        aux.connect(new InetSocketAddress("arduino.local", 65000), 500);
      } catch (IOException ex) {
        //If the connection fails, try to connect to the arduino router ip
        System.out.println("Fail to connect to arduino.local\n"
                           + ex.getMessage());
        try {
          aux = new Socket();
          aux.connect(new InetSocketAddress("192.168.240.1", 65000), 500);
        } catch (IOException ex2) {
          //if the connection fails again
          //enter in the no connection mode, and stop this thread
          throw new IIOException("Fail to connect to arduino router ip\n"
                                 + ex2.getMessage());
        }
      }

      //Get the buffers with the connection
      this.arduinoSocket = aux;
      this.in = new BufferedReader(new InputStreamReader(arduinoSocket
                                                         .getInputStream()));
      this.out = new PrintWriter(arduinoSocket.getOutputStream());
      this.available = false;
    }

    /**
     * @return The data readed
     */
    public synchronized int nextInt () {
      this.available = false;
      return this.data;
    }

    /**
     * @return true if there is new data ready, else false
     */
    public synchronized boolean isAvailable () {
      return this.available;
    }

    /**
     * This method reads the data sent by the glove
     */
    @Override
    public void run() {
      String inputData;

      //Runs forever since the start of the program until it's shutdown
      while (keepRunning) {
        try {
          //Tries to read the data from the socket
          inputData = this.in.readLine();

          //If the data isn't null
          if (inputData != null) {
            this.data = Integer.parseInt(inputData); //Set the data
            this.available = true; //Set avaliable as true
          }
        } catch (IOException ex) {}
      }
    }

    /**
     * Close the socket with the glove
     * @see Reader#stop()
     */
    private void stop () {
      try {
        this.out.println(0);
        this.out.close();
        this.in.close();
        this.arduinoSocket.close();
      } catch (IOException ex) {
        System.err.println("Error while closing the socket");
        System.err.println(ex.getMessage());
      }
    }
  }
}
