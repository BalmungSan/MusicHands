package org.musichands.musichands.backend;

import java.io.File;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.musichands.musichands.gui.MainInterface;
import org.musichands.musichands.movements.Movement;
import org.musichands.musichands.movements.MovementID;
import org.musichands.musichands.movements.actions.Action;
import org.musichands.musichands.movements.actions.ActivitiesXMLFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * This class is used to manage the user profiles file (XML File)
 * This class use the dom parser
 * This class is a singleton
 * @author Luis Miguel Mejía Suárez
 * @see Document
 */
public class ProfilesManager {
  //The unique instance of this class
  private static volatile ProfilesManager INSTANCE = null;

  /**
   * @return The instance of this class
   */
  public static synchronized ProfilesManager getInstance () {
    try {
      //If is the first time this method is called, create the instance
      //Open the deafult profiles file
      String actualDir =
        URLDecoder.decode(new File(System.getProperty("java.class.path"))
                          .getParent(), "UTF-8");
      if (INSTANCE == null) INSTANCE =
                              new ProfilesManager(actualDir + "/profiles.xml");
    } catch (Exception ex) {
      //Show an error if there is a problem reading the profiles file
      System.err.println("Error reading the profiles file\n" + ex.getMessage());
    }

    return INSTANCE;
  }

  //The loaded profiles file
  private final Document profiles;

  //The path to the file in disk
  private final String profilesFilePath;

  //A factory to create actions
  private final ActivitiesXMLFactory actionsFactory;

  /**
   * Open, normalize and validate the profiles file
   * The schema is the file "/resources/profiles.xsd"
   * Read this for know why normalize
   * http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
   * @param profilesFilePath the path to the profiles file
   * @see Schema
   * @see DocumentBuilderFactory#setSchema(javax.xml.validation.Schema)
   * @see DocumentBuilderFactory#DocumentBuilderFactory()
   * @see javax.xml.parsers.DocumentBuilder#parse(java.lang.String)
   * @see Document
   * @see Document#normalize()
   * @see ActivitiesXMLFactory
   */
  private ProfilesManager (String profilesFilePath) throws Exception {
    //Save the profiles file path
    this.profilesFilePath = profilesFilePath;

    //Validate the xml file with the profiles schema
    Schema profilesSchema =
      SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
      .newSchema(this.getClass().getResource("/resources/profiles.xsd"));
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    factory.setValidating(false);
    factory.setNamespaceAware(true);
    factory.setSchema(profilesSchema);

    //Open the xml file
    profiles = factory.newDocumentBuilder().parse(profilesFilePath);

    //Normalize the xml file
    profiles.getDocumentElement().normalize();

    //Create a new factory
    actionsFactory = new ActivitiesXMLFactory();
  }

  /**
   * This method retrives all the nodes with the tag "profile"
   * and adds its id (name) to the list of profiles
   * @return a list with all the user profiles names
   * @see Document#getElementsByTagName(java.lang.String)
   * @see List
   */
  public List<String> getAllProfiles () {
    List<String> IDs = new ArrayList();

    NodeList profilesTags = profiles.getElementsByTagName("profile");
    for (int i = 0; i < profilesTags.getLength(); i++) {
      IDs.add(profilesTags.item(i).getAttributes()
              .getNamedItem("name").getTextContent());
    }

    return IDs;
  }

  /**
   * Loads the profile to memory
   * This method get the child profile by id profileName
   * and loads its configuration
   * @param profileName the name of the profile to load (id)
   * @see Controller#addActions(org.musichands.musichands.movements.MovementID,
   * java.util.List)
   * @see MainInterface#addActions(org.musichands.musichands.movements.MovementID,
   * java.util.List)
   * @see Document#getElementById(java.lang.String)
   */
  public void loadProfile (String profileName) {
    //Get the child profile by id profileName
    Element profile = profiles.getElementById(profileName);

    //If that id didn't exits exit
    if (profile == null) return;

    //Get all the movements of that profile
    NodeList configuration = profile.getElementsByTagName("movement");
    int movementID;
    Element movement;
    Element actionElement;
    NodeList actions;
    List<Action> actionsList;
    Action action;
    String actionName;
    String data;

    //For each movement in the configuration...
    for (int i = 0; i < configuration.getLength(); i++) {
      //... get the id of the movement
      movement = (Element) configuration.item(i);
      movementID = Integer.parseInt(movement.getAttributes().item(0)
                                    .getTextContent());

      //and get all the actions of that movement...
      actionsList = new ArrayList();
      actions = movement.getElementsByTagName("action");
      for (int j = 0; j < actions.getLength(); j++) {
        //... create a new Action for every action
        actionElement = (Element) actions.item(j);
        actionName =
          actionElement.getElementsByTagName("name").item(0).getTextContent();
        data =
          actionElement.getElementsByTagName("data").item(0).getTextContent();
        action = actionsFactory.createAction(actionName + "-" + data);
        actionsList.add(action);
      }

      //finally load the movement in memory and in the interface
      Controller.addActions(MovementID.getMovement(movementID), actionsList);
      MainInterface.getInstance().addActions(MovementID.getMovement(movementID),
                                             actionsList);
    }
  }

  /**
   * Save the actual configuration to a profile
   * @param profileName the name of the profile to save the configuration (id)
   * @param conf the configuration to save
   * @see Controller#MAP
   * @see Map#entrySet()
   * @see MovementID#getID()
   * @see Movement#getActios()
   * @see Document#getElementById(java.lang.String)
   * @see Node#appendChild(org.w3c.dom.Node)
   * @see Node#replaceChild(org.w3c.dom.Node, org.w3c.dom.Node)
   */
  public void saveProfile (String profileName, Map<MovementID, Movement> conf) {
    //Get the old profile node by its id (profileName)
    Element oldProfile = profiles.getElementById(profileName);

    //Construct the new profile ------------------------------------------------
    Element newProfile = profiles.createElement("profile");

    //Set the id of this node
    newProfile.setAttribute("name", profileName);
    newProfile.setIdAttribute("name", true);

    //For each movement in the map...
    Element movementTag;
    Element actionTag;
    Element actionNameTag;
    Element actionDataTag;
    for (Map.Entry<MovementID, Movement>  movement : conf.entrySet()) {
      //... create a new movement tag with a 'movementID' attribute
      movementTag = profiles.createElement("movement");
      movementTag.setAttribute("movementID",
                               String.valueOf(movement.getKey().getID()));

      //for every action in movement...
      for (Action action : movement.getValue().getActios()) {
        //... create the tag action
        actionTag = profiles.createElement("action");

        //create the tag name, with the action name
        actionNameTag = profiles.createElement("name");
        actionNameTag.appendChild(profiles.createTextNode(action.getClass()
                                                          .getSimpleName()));
        actionTag.appendChild(actionNameTag);

        //create the tag data, with the action constructor parameter
        actionDataTag = profiles.createElement("data");
        actionDataTag.appendChild(profiles.createTextNode(action
                                                          .getParameter()));
        actionTag.appendChild(actionDataTag);

        //add the actions to the movement
        movementTag.appendChild(actionTag);
      }

      //
      newProfile.appendChild(movementTag);
    }
    //--------------------------------------------------------------------------

    //If the old profile exits ...
    if (oldProfile != null) {
      //... replace the old profile with the new profile
      oldProfile.getParentNode().replaceChild(newProfile, oldProfile);
    } else {
      //... if no, insert the new node
      profiles.getLastChild().appendChild(newProfile);
    }
  }

  /**
   * Remove a profile from the profiles file
   * @param profileName the name of the profile to delete (id)
   * @see Document#getElementById(java.lang.String)
   * @see Node#removeChild(org.w3c.dom.Node)
   */
  public void deleteProfile (String profileName) {
    Element profile = profiles.getElementById(profileName);
    if (profile != null) profile.getParentNode().removeChild(profile);
  }

  /**
   * Close and save the file in disk
   */
  public void close () {
    try {
      profiles.normalizeDocument();
      Transformer transformer = TransformerFactory.newInstance().newTransformer();
      DOMSource source = new DOMSource(profiles);
      StreamResult result = new StreamResult(new File(profilesFilePath));
      transformer.transform(source, result);
    } catch (TransformerException | TransformerFactoryConfigurationError ex) {
      System.err.println("Error saving the profiles file\n" + ex.getMessage());
    }
  }
}
