package org.musichands.musichands.backend;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.musichands.musichands.gui.ModuleDialog;

/**
 * This class is used to download and install a module
 * This class is a Thread
 * @author Luis Miguel Mejía Suárez
 * @see Runnable
 * @see ModuleDialog
 */
public class ModuleInstaller implements Runnable {
  //The module purchase code
  private final String moduleCode;

  //The server to get the module
  private final String server;

  //The dialog to show messages
  private final ModuleDialog dialog;

  /**
   * Constructor of the installer
   * Receives the module purchase code and the dialog to show messages
   * Initialice server to the deafult (musichands.comxa.com)
   * @param moduleCode The module purchase code
   * @param dialog The dialog
   */
  public ModuleInstaller (String moduleCode, ModuleDialog dialog) {
    this.dialog = dialog;
    this.moduleCode = moduleCode;
    this.server = "musichands.comxa.com";
  }

  /**
   * Download the module specified by the module purchase code in background
   * @see DatabaseManager#getModuleLink(java.lang.String)
   * @see FTPUtil#downloadDirectory(org.apache.commons.net.ftp.FTPClient,
   * java.lang.String, java.lang.String, java.lang.String)
   */
  @Override
  public void run() {
    //Creates a new FTPClient to download the module for the server
    FTPClient client = new FTPClient();

    try {
      //Get the module link
      dialog.setText("Checking the module purchase code");
      String moduleLink = new DatabaseManager(server).getModuleLink(moduleCode);

      //Get the jar name and the directory where it is
      String jarFile =
        URLDecoder.decode(new File(System.getProperty("java.class.path"))
                          .getAbsolutePath(), "UTF-8");
      String actualDir =
        URLDecoder.decode(new File(jarFile).getParent(), "UTF-8");

      //Donwload the module
      dialog.setText("Downloading the module");
      client.connect(this.server);
      client.login("a4312417", "josalu16");
      FTPUtil.downloadDirectory(client, moduleLink, "", actualDir + "/temp");

      //Modify the downloaded module file, to add the already installed modules
      InputStream oldModules =
        this.getClass().getResourceAsStream("/resources/modules");
      File newModules = new File(actualDir + "/temp" + moduleLink + "/resources/modules");
      FileUtils.write(newModules, "\n"+IOUtils.toString(oldModules, "UTF-8"),
                      "UTF-8", true);
      oldModules.close();

      //Launch the installer
      String commands[] = new String[]{"java", "-jar", "Installer.jar", jarFile,
                                       actualDir, "/temp" + moduleLink};
      Runtime.getRuntime().exec(commands, null, new File(actualDir));

      //Stop this program
      Controller.stop();
    } catch (IOException ioex) {
      dialog.setText("<html>Error downloading the module<br>"
                     + ioex.getMessage() + "</html>");
    } catch (ModuleException mex) {
      dialog.setText(mex.getMessage());
    } finally {
      try {
        dialog.enableDialog();
        client.logout();
        client.disconnect();
      } catch (IOException ex) {
        System.err.println(ex.getMessage());
      }
    }
  }
}
