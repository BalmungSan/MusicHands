package org.musichands.musichands.backend;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.musichands.musichands.gui.MainInterface;
import org.musichands.musichands.movements.Movement;
import org.musichands.musichands.movements.MovementID;
import org.musichands.musichands.movements.actions.Action;
import org.musichands.musichands.wifi.Parser;

/**
 * This class is in charge of being the only point of access to the model and
 * to control the execution workflow of the program.
 * This class only provide public static methods
 *
 * @author Luis Miguel Mejía Suárez
 */
public class Controller {
  //Map to associate every Movement ID (@link MovementID)
  //with its object in memory (@link Movement)
  private static final Map<MovementID, Movement> MAP =
    new EnumMap(MovementID.class);

  //The connection with the glove(s) by wifi
  private static final Parser PARSER = Parser.getInstance();

  //The point of access to the profiles file
  private static final ProfilesManager PROFILES = ProfilesManager.getInstance();

  //This flag controls when the program it's in play or in pause
  private static volatile boolean run;

  //This flag indicates when a connection could be established with the glove
  private static volatile boolean connected;

  /**
   * @return The state of tthe program. true = play, false = pause
   */
  public static synchronized boolean isRunning() {
    return run;
  }

  /**
   * Start the execution of the program
   * Start a new thread for the WifiConnnector
   * @see Parser
   */
  public static void start () {
    System.out.println("Execution started");
    MovementID.createMap();
    run = false;
    new Thread(PARSER).start();
  }

  /**
   * Stop the program and exit normally
   * @see Parser#stop()
   */
  public static void stop () {
    try {
      run = false;
      if (connected) PARSER.stop();
      PROFILES.close();
      System.out.println("Execution stopped");
      Thread.sleep(1000);
    } catch (InterruptedException ex) {
      System.err.println("Error: " + ex.getMessage());
    } finally {
      //Exit
      System.exit(0);
    }
  }

  /**
   * Change the state of the program
   * Play <-> Pause
   * @see MainInterface#changeState(boolean)
   */
  public static synchronized void pause_play () {
    run = !run;
    MainInterface.getInstance().changeState(!run);
  }

  /**
   * this method set the connection flag to fail (false)
   * @see #connected
   * @see #connection()
   */
  public static synchronized void noConnection () {
    connected = false;
  }

  /**
   * this method set the connection flag to success (true)
   * @see #connected
   * @see #noConnection()
   */
  public static synchronized void connection () {
    connected = true;
  }
  /**
   * get if the connection could be established
   * @return true if the connection success, false if not
   * @see #connected
   */
  public static synchronized boolean  isConnected () {
    return connected;
  }

  /**
   *
   */

  /**
   * @return a list with all the user profiles names
   * @see ProfilesManager#getAllProfiles()
   * @see List
   */
  public static List<String> getAllProfiles () {
    return PROFILES.getAllProfiles();
  }

  /**
   * Load a user profile
   * @param profileName the name of the profile to load
   * @see ProfilesManager#loadProfile(java.lang.String)
   */
  public static void loadProfile (String profileName) {
    if (profileName != null) PROFILES.loadProfile(profileName);
  }

  /**
   * Save the actual configuration of the program
   * @param profileName the name of the profile to save
   * @see #MAP
   * @see ProfilesManager#saveProfile(java.lang.String, java.util.Map)
   */
  public static void saveProfile (String profileName) {
    if (profileName != null) PROFILES.saveProfile(profileName, MAP);
  }

  /**
   * Remove a profile from profiles
   * @param profileName the name of the profile to delete
   * @see ProfilesManager#deleteProfile(java.lang.String)
   */
  public static void deleteProfile (String profileName) {
    if (profileName != null) PROFILES.deleteProfile(profileName);
  }

  /**
   * Clear the actual configuration
   * @see Map#clear()
   */
  public static void clear () {
    MAP.clear();
  }

  /**
   * @return A set with the id of the movements that are actually in the program
   * @see Map#keySet()
   * @see MovementID
   */
  public static Set<MovementID> getValidMovements () {
    return MAP.keySet();
  }

  /**
   * Get the movement with its id
   * @param movementID The id of the movement
   * @return The movement or null if the movement wasn't create before
   * @see MovementID
   * @see Movement
   */
  public static Movement getMovement (MovementID movementID) {
    return MAP.get(movementID);
  }

  /**
   * Add a list of actions to a movement
   * First checks if the movement already exist
   * if not, creates the movement
   * @param movementID The id of the movement
   * @param actions The List of actions to add
   * @see MovementID
   * @see Action
   * @see Movement#addActions(java.util.List)
   */
  public static void addActions (MovementID movementID, List<Action> actions) {
    //Get the movement from the map
    Movement movement = getMovement(movementID);

    //If the movement hasn't been created
    if (movement == null) {
      //It is created and saved on the map
      movement = Movement.MovementsFactory.createMovement(movementID);
      MAP.put(movementID, movement);
    }

    //Add the action to the movement
    movement.addActions(actions);
  }

  /**
   * Remove an action of a given movement
   * @param movementID The ID of the movement to remove the action
   * @param action The action to remove
   */
  public static void removeAction (MovementID movementID, Action action) {
    //If after remove the action the movement hasn't more actions...
    if (MAP.get(movementID).removeAction(action) == 1) {
      //... remove the movement from the map
      MAP.remove(movementID);
    }
  }
}
