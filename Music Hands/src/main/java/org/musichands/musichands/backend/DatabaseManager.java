package org.musichands.musichands.backend;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * This class is in charge of manage all the operation with the database
 * the operations are made by PHP scripts in the server
 * @author Luis Miguel Mejía Suárez
 */
public class DatabaseManager {
  //The server to connect
  private final String server;

  //The User Agent to use in the connections
  private final String userAgent;

  /**
   * Construct a new instance of Database Manager that will connect to the
   * specified server and user the default user agent (Mozilla/5.0)
   * @param server the server to connect
   */
  public DatabaseManager (String server) {
    this.server = "http://" + server;
    this.userAgent = "Mozilla/5.0";
  }

  /**
   * Gets the module's link given its purchase code
   * @param moduleCode the module's purchase code
   * @return the link of the module
   * @throws org.musichands.musichands.backend.ModuleException
   *  - if an error ocurred while trying to get the link
   */
  public String getModuleLink (String moduleCode) throws ModuleException {
    String result = "";

    try {
      //Call the php script
      URL url = new URL(server + "/scripts/getModule.php");
      HttpURLConnection con = (HttpURLConnection) url.openConnection();
      con.setRequestMethod("POST");
      con.setRequestProperty("User-Agent", userAgent);
      con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
      con.setDoOutput(true);

      //Add the arguments
      DataOutputStream wr = new DataOutputStream(con.getOutputStream());
      wr.writeBytes("data=" + moduleCode);
      wr.flush();
      wr.close();

      //Get the response (ModuleLink or ERROR)
      BufferedReader in =
        new BufferedReader(new InputStreamReader(con.getInputStream()));
      result = in.readLine();
      in.close();
    } catch (IOException ex) {
      //If an error occurred throw a new ModuleException
      throw new ModuleException("ERROR: occurred during the execution"
                                + " of the scriptt" + ex.getMessage());
    }
    //If the response of the server starts with "ERROR"
    //throw a new ModuleException
    if (result.startsWith("ERROR:")) throw new ModuleException(result);

    //If not, return the module link
    return result;
  }
}

/**
 * This class is an exception when cannot get the module link
 * @author Luis Miguel Mejía Suárez
 */
class ModuleException extends Exception {
  /**
   * Constructor of the exception
   * Receives a message to show in the interface
   * The message will be formated to html
   * @param message The message to show
   * @see Exception#Exception(java.lang.String)
   */
  public ModuleException (String message) {
    super("<html>" + message.replaceAll("\n", "<br>") + "</html>");
  }
}
