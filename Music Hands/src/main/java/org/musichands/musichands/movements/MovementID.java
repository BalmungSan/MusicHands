package org.musichands.musichands.movements;

import java.util.HashMap;
import java.util.Map;
import javax.swing.ImageIcon;

/**
 * Enum file with all the movements detectable by the glove
 * @author Luis Miguel Mejía Suárez
 */
public enum MovementID {
  IndexButton1(0, "Activa esta acción al presionar el botón que se encuentra "
               + "en la punta del dedo índice"),
  IndexButton2(1, "Activa esta acción al presionar el botón que se encuentra "
               + "en el medio del dedo índice"),
  IndexButton3(2, "Activa esta acción al presionar el botón que se encuentra "
               + "en la parte baja del dedo índice"),
  MiddleButton1(3, "Activa esta acción al presionar el botón que se encuentra "
                + "en la punta del dedo corazón"),
  MiddleButton2(4, "Activa esta acción al presionar el botón que se encuentra "
                + "en el medio del dedo corazón"),
  MiddleButton3(5, "Activa esta acción al presionar el botón que se encuentra "
                + "en la parte baja del dedo corazón"),
  RingButton1(6, "Activa esta acción al presionar el botón que se encuentra "
              + "en la punta del dedo anular"),
  RingButton2(7, "Activa esta acción al presionar el botón que se encuentra "
              + "en el medio del dedo anular"),
  RingButton3(8, "Activa esta acción al presionar el botón que se encuentra"
              + "en la parte baja del dedo anular"),
  IndexDown(9, "Activa esta acción al bajar el dedo índice"),
  IndexUp(10, "Activa esta acción al subir el dedo índice"),
  MiddleDown(11, "Activa esta acción al bajar el dedo corazón"),
  MiddleUp(12, "Activa esta acción al subir el dedo corazón"),
  RingDown(13, "Activa esta acción al bajar el dedo anular"),
  RingUp(14, "Activa esta acción al subir el dedo anular"),
  JoystickUp(15, "Activa esta acción al mover el joystick hacía arriba"),
  JoystickRight(16, "Activa esta acción al mover el joystick hacía la derecha"),
  JoystickDown(17, "Activa esta acción al mover el joystick hacía abajo"),
  JoystickLeft(18, "Activa esta acción al mover el joystick hacía la izquierda");

  //The ID number for this MovementID
  private final int id;

  //The description of this movement
  private final String description;

  //The image of this movement
  private final ImageIcon movementImage;

  //Map to associate every MovementID with its id number
  private final static Map<Integer, MovementID> MAP = new HashMap();

  /**
   * Constructor of the movementID
   * @param id the id number
   * @param description the description of the movement
   */
  private MovementID(int id, String description) {
    //ID
    this.id = id;

    //Description
    this.description = "<html><b>" + description + "</b></html>";

    //Load the image of this movement
    this.movementImage = new ImageIcon(this.getClass()
                                       .getResource("/resources/images/"
                                                    + this.name() + ".png"));
  }

  /**
   * Add every MovementID to the map
   * called only once in the application starup
   */
  public static void createMap () {
    for (MovementID m : MovementID.values()) {
      MAP.put(m.id, m);
    }
  }

  /**
   * Get a movementID
   * @param id The id of the movement
   * @return The movementID
   */
  public static MovementID getMovement (int id) {
    return MAP.get(id);
  }

  /**
   * @return the id number
   */
  public int getID () {
    return this.id;
  }

  /**
   * @return the description
   */
  public String getDescription () {
    return this.description;
  }

  /**
   * @return the image of this movement
   * @see ImageIcon
   */
  public ImageIcon getMovementImage () {
    return this.movementImage;
  }
}
