package org.musichands.musichands.movements.actions;

import java.util.ArrayList;
import java.util.List;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;

/**
 * This class is used to control all the clips of the program
 * @author Luis Miguel Mejía Suárez
 * @see Clip
 * @see Sound
 * @see VolumeUp
 * @see VolumeDown
 */
public class SoundControl {
  //All the clips in the program
  private static final List<Clip> CLIPS = new ArrayList();

  /**
   * The FloatControl Type of the volume of every clip
   * @see javax.sound.sampled.FloatControl.Type#MASTER_GAIN
   */
  public static final FloatControl.Type VOLUME = FloatControl.Type.MASTER_GAIN;

  /**
   * @return a new clip saved in CLIPS
   * @see Clip
   * @see AudioSystem#getClip()
   */
  public static synchronized Clip getClip () {
    Clip clip = null;

    try {
      clip = AudioSystem.getClip();
      CLIPS.add(clip);
    } catch (LineUnavailableException ex) {
      System.err.println("ERROR: " + ex.getMessage());
      System.exit(0);
    }

    return clip;
  }

  /**
   * Change the volume of the application
   * @param value increase/decrease the volume by value
   */
  public static synchronized void changeVloume (int value) {
    FloatControl fc;

    for (Clip clip : CLIPS) {
      fc = (FloatControl) clip.getControl(VOLUME);
      fc.setValue(fc.getValue() + value);
    }
  }
}
