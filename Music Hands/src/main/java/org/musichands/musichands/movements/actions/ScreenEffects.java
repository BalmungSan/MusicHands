package org.musichands.musichands.movements.actions;

import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;
import org.musichands.musichands.gui.Screen;

/**
 * This action is used to show images in screen
 * @see Action
 * @see Screen
 * @see ImageIcon
 */
public class ScreenEffects extends Action {
  //The image of this Action
  private final ImageIcon image;

  /**
   * Constructor
   * Call the Action constructor with "Image: " + path
   * if path is "deafult image" load the logo
   * @param path the path of the image
   * @see Action#Action(java.lang.String, java.lang.String)
   */
  protected ScreenEffects (String path) {
    super("Image: " +  path, path);

    if (path.equals("default image")) {
      this.image = new ImageIcon(this.getClass()
                                 .getResource("/resources/images/logo.jpg"));
    } else {
      this.image = new ImageIcon(path);
    }
  }

  /**
   * Show this image in screen
   * send a new runnable to the EDT
   * @see Screen#getInstance()
   * @see Screen#getImageLabel()
   * @see javax.swing.JLabel#setIcon(javax.swing.Icon)
   * @see SwingUtilities#invokeLater(java.lang.Runnable)
   */
  @Override
  protected void execute () {
    //Sends a new runnable to the EDT
    SwingUtilities.invokeLater(new Runnable() {
        @Override
        public void run () {
          //Get the unique instance of Screen
          Screen screen = Screen.getInstance();

          //Scale the image to the size of screen
          ImageIcon scaledImage = new ImageIcon
            (image.getImage().getScaledInstance(screen.getBounds().width,
                                                screen.getBounds().height,
                                                Image.SCALE_REPLICATE));

          //Change the image of screen
          screen.getImageLabel().setIcon(scaledImage);
          screen.setVisible(true);
        }
      });
  }
}
