package org.musichands.musichands.movements.actions;

import org.musichands.musichands.backend.Controller;

/**
 * This action is used to pause/unpause the program
 * @author Luis Miguel Mejía Suárez
 * @see Action
 */
public class PausePlay extends Action {
  /**
   * Constructor of the class
   * Call the constructor of Action with "Pause / Play"
   * @see Action#Action(java.lang.String, java.lang.String)
   */
  protected PausePlay () {
    super("Pause / Play", "");
  }

  /**
   * Override the Run method on Action
   * To excecute this action even if the program state is pause
   * @see Controller#isRunning()
   * @see Action#run()
   */
  @Override
  public void run () {
    execute();
  }

  /**
   * Pause/Unpasue the program
   * @see Controller#pause_play()
   */
  @Override
  protected void execute () {
    Controller.pause_play();
  }
}
