package org.musichands.musichands.movements.actions;

/**
 * Template for every factory of actions
 * @author Luis Miguel Mejía Suárez
 * @see Action
 */
public interface ActionsFactory {
  /**
   * creates a new Action in base of an input
   * @param data the data to create the action
   * @return the created action
   * @see Action
   */
  public Action createAction (String data);
}
