package org.musichands.musichands.movements.actions;

import org.musichands.musichands.backend.Controller;

/**
 * Abstract representation of every action
 * @author Luis Miguel Mejía Suárez
 */
public abstract class Action implements Runnable {
  //The name of this action
  private final String name;

  //parameter passed to the constructor of this action
  private final String parameter;

  /**
   * Constructor of the action
   * Can only be called for implementations of this class
   * @param name the name of the action
   * @param parameter the parameter passed to the constructor of this action
   */
  protected Action (String name, String parameter) {
    this.name = name;
    this.parameter = parameter;
  }

  /**
   * @return the name of this action
   */
  public synchronized String getName () {
    return this.name;
  }

  /**
   *
   */
  public synchronized String getParameter () {
    return this.parameter;
  }

  /**
   * All the action logic goes here
   */
  protected abstract void execute ();

  /**
   * Only run this action if the program state is play
   * @see Controller#isRunning()
   * @see Thread#run()
   */
  @Override
  public void run () {
    if (Controller.isRunning()) {
      synchronized (this) {
        execute();
        notifyAll();
      }
    }
  }
}
