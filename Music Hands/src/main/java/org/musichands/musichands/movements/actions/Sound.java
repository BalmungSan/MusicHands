package org.musichands.musichands.movements.actions;

import java.io.BufferedInputStream;
import java.io.IOException;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * This action is used to play sounds
 * @author Luis Miguel Mejía Suárez
 * @see Action
 * @see Clip
 * @see LineListener
 */
public class Sound extends Action implements LineListener {
  //Flag to determine when the sound was completed
  private volatile boolean playCompleted;

  //The sound of this Action
  private final Clip clip;

  /**
   * Constructor of the class, receives the pah to the sound
   * call the constructor of action with "Sound: [module]/[sound]"
   * @param path the path to the sound file
   * @see Action#Action(java.lang.String, java.lang.String)
   * @see SoundControl#getClip()
   */
  protected Sound (String path) {
    //Call the constructor of action with "Sound: [module]/[sound]"
    super("Sound: " + path.substring(18, path.lastIndexOf(".")), path);

    //Get a new Clip from SoundControl
    clip = SoundControl.getClip();

    try {
      //Open the clip with the sound file specified by path
      clip.addLineListener(this);
      clip.open(AudioSystem.getAudioInputStream
                (new BufferedInputStream(this.getClass()
                                         .getResourceAsStream(path))));

      //Set the start volume of the clip to -20
      FloatControl fc = (FloatControl) clip.getControl(SoundControl.VOLUME);
      fc.setValue(-20);
    } catch (IOException | UnsupportedAudioFileException |
             LineUnavailableException ex) {
      System.err.println("Error with audio file '" + path
                         + "\n" + ex.getLocalizedMessage());
    }
  }

  /**
   * Start playing this sound
   */
  @Override
  protected void execute () {
    //Initiate the sound
    playCompleted = false;
    clip.setFramePosition(0);
    clip.start();

    //Wait until its completed
    while (!playCompleted) {}

    //Close the clip
    clip.flush();
    clip.stop();
  }

  /**
   * This method handle all the event that occur in this clip.
   * If the event is {@link javax.sound.sampled.LineEvent.Type#STOP}
   * set {@link Sound#playCompleted} true
   * @param event the event received
   */
  @Override
  public void update (LineEvent event) {
    if (event.getType() == LineEvent.Type.STOP) {
      playCompleted = true;
    }
  }
}
