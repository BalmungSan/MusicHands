package org.musichands.musichands.movements.actions;

/**
 * This action is used to decrease the applications volume
 * @author Luis Miguel Mejía Suárez
 * @see Action
 */
public class VolumeDown extends Action {
  /**
   * Constructor of the class
   * Calls the constructor of action with "Volume down"
   * @see Action#Action(java.lang.String, java.lang.String)
   */
  protected VolumeDown () {
    super("Volume down", "");
  }

  /**
   * Decrease the applications volume by 5
   * @see SoundControl#changeVloume(int)
   */
  @Override
  protected void execute() {
    SoundControl.changeVloume(-5);
  }
}
