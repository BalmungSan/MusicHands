package org.musichands.musichands.movements;

import java.util.ArrayList;
import java.util.List;
import org.musichands.musichands.movements.actions.Action;

/**
 * This class is an abstract representation of the movements
 * the glove can detect. Every movement has a list of actions
 * that have to be performed when the movement is activated
 * @author Luis Miguel Mejía Suárez
 * @see Action
 * @see List
 */
public class Movement {
  //The list of actions this movement was associated
  private final List<Action> actions;

  //The id number of this Movement, @see MovementID#MovementID(int)
  private final int id;

  /**
   * Default constructor of the class
   * Initialize an empty List as the list of action
   * we use an ArrayList because it's the one with the best performance
   * in the get() method
   * @param id The id number of this movement
   * @see MovementID#id
   * @see List
   * @see ArrayList
   */
  private Movement (int id) {
    this.actions = new ArrayList<>();
    this.id = id;
  }

  /**
   * Add an action to the list of actions
   * @param actions the List of actions to be added
   * @see Action
   */
  public void addActions (List<Action> actions) {
    this.actions.addAll(actions);
  }

  /**
   * Remove an specific action from the list
   * @param action The action to be removed
   * @return 1 if this movement now doesn't have more actions else 0
   * @see Action
   */
  public int removeAction (Action action) {
    this.actions.remove(action);

    if (this.actions.isEmpty()) return 1;
    else return 0;
  }

  /**
   * @return The list of actions associated to this movement
   * @see List
   */
  public List<Action> getActios () {
    return this.actions;
  }

  /**
   * @return the id number of this movement
   * @see MovementID#id
   */
  public int getID () {
    return this.id;
  }

  /**
   * Static helper class
   * This class is a factory of movements
   * @author Luis Miguel Mejía Suárez
   * @see Movement
   * @see MovementID
   */
  public static class MovementsFactory {
    /**
     * Create a new movement in base of the id received
     * @param movementID the id of the movement
     * @return the movement
     * @see Movement#Movement(int)
     * @see MovementID#getID()
     */
    public static Movement createMovement (MovementID movementID) {
      return new Movement(movementID.getID());
    }
  }
}
