package org.musichands.musichands.movements.actions;

/**
 * Deafult implementation of ActionsFactory
 * @author Luis Miguel Mejía Suárez
 * @see ActionsFactory
 * @see Action
 */
public class ActivitiesFactory implements ActionsFactory {
  /**
   * Creates a new Action in base of data
   * data -> Action
   * Pause/Play -> PausePlay()
   * Volume up -> VolumeUp()
   * Volume down -> VolumeDown()
   * Image -> ScreenEffects("default image")
   * Default -> Sound(data)
   * @param data the data to create the movement
   * @return the created action
   * @see PausePlay
   * @see VolumeUp
   * @see VolumeDown
   * @see ScreenEffects#ScreenEffects(java.lang.String)
   * @see Sound#Sound(java.lang.String)
   */
  @Override
  public Action createAction(String data) {
    Action action;

    switch (data) {
    case "Pause/Play":
      action = new PausePlay();
      break;
    case "Volume up":
      action = new VolumeUp();
      break;
    case "Volume down":
      action = new VolumeDown();
      break;
    case "Image":
      action = new ScreenEffects("default image");
      break;
    default:
      action = new Sound(data);
      break;
    }

    return action;
  }
}
