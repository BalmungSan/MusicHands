package org.musichands.musichands.movements.actions;

/**
 * This action is used to increase the applications volume
 * @author Luis Miguel Mejía Suárez
 * @see Action
 */
public class VolumeUp extends Action {
  /**
   * Constructor of the class
   * Calls the constructor of action with "Volume up"
   * @see Action#Action(java.lang.String, java.lang.String)
   */
  protected VolumeUp () {
    super("Volume up", "");
  }

  /**
   * Increase the applications volume by 5
   * @see SoundControl#changeVloume(int)
   */
  @Override
  protected void execute() {
    SoundControl.changeVloume(5);
  }
}
