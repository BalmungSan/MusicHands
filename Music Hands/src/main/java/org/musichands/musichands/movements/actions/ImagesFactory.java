package org.musichands.musichands.movements.actions;

/**
 * An implementation of actions factory for images
 * @author Luis Miguel Mejía Suárez
 * @see ActionsFactory
 * @see ScreenEffects
 * @see org.musichands.musichands.gui.ActionPanel.ActionPanelBuilder#createImagePanel(org.musichands.musichands.movements.actions.Action) 
 */
public class ImagesFactory implements ActionsFactory {
  /**
   * Create a new ScreeEffects instance with data
   * @param data the image path
   * @return the created ScreeEffects action
   * @see ScreenEffects#ScreenEffects(java.lang.String)
   */
  @Override
  public Action createAction(String data) {
    return new ScreenEffects(data);
  }
}
