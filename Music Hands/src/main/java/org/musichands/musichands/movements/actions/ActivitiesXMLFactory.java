package org.musichands.musichands.movements.actions;

/**
 * An implementation of actions factory for the xml profiles file
 * @author Luis Miguel Mejía Suárez
 * @see ActionsFactory
 * @see Action
 * @see org.musichands.musichands.backend.ProfilesManager
 */
public class ActivitiesXMLFactory implements ActionsFactory {
  /**
   * Create a new action in base of data
   * data = [actionName]-[parameter]
   * actionName -> Action
   * Pause/Play -> PausePlay()
   * VolumeUp -> VolumeUp()
   * VolumeDown -> VolumeDown()
   * ScreenEffects -> ScreenEffects(parameter)
   * Sound -> Sound(parameter)
   * @param data the information to create the action
   * @return the created action
   * @see PausePlay
   * @see VolumeUp
   * @see VolumeDown
   * @see ScreenEffects#ScreenEffects(java.lang.String)
   * @see Sound#Sound(java.lang.String)
   */
  @Override
  public Action createAction(String data) {
    String actionName = data.substring(0, data.indexOf("-"));
    String parameter = data.substring(data.indexOf("-") + 1);
    Action action;

    switch (actionName) {
    case "PausePlay":
      action = new PausePlay();
      break;
    case "VolumeUp":
      action = new VolumeUp();
      break;
    case "VolumeDown":
      action = new VolumeDown();
      break;
    case "ScreenEffects":
      action = new ScreenEffects(parameter);
      break;
    case "Sound":
      action = new Sound(parameter);
      break;
    default:
      action = null;
      break;
    }

    return action;
  }
}
