<?xml version="1.0" encoding="UTF-8"?>

<!-- Pom file to compile the project using maven
 
Music Hands: The idea consists in the realization of a device which is placed on the user's hands in order to sense the different positions of the user hands
             to subsequently send this information to a program that has the ability to interpret this data and perform various actions 
             that were previously set by the user
 
Authors:
   Alejandro Salgado Gómez 
   Luis Miguel Mejía Suárez
   Johan Sebastian Yepes Rios

Version: 3.1.0-FINAL_RELEASE (06/06/2016)
-->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>org.musichands</groupId>
    <artifactId>MusicHands</artifactId>
    <version>3.1.0-FINAL_RELEASE</version>
    <packaging>jar</packaging>
    <url>http://musichands.comxa.com/</url>
    <name>MusicHands</name>
    
    <!-- General properties. They will be applied to all included files -->
    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
    </properties>
  
    <!-- Dependencies of the project -->
    <dependencies>
        
        <!-- Junit to make test 
        Remark: Even if this project don't run test, leave it
        Maven needs it to compile -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
        </dependency>
        
        <!-- Looger -->
        <dependency>
            <groupId>log4j</groupId>
            <artifactId>log4j</artifactId>
            <version>1.2.12</version>
            <scope>compile</scope>
        </dependency>
        
        <!-- Apache Commons net to download FTP files -->
        <dependency>
            <groupId>commons-net</groupId>
            <artifactId>commons-net</artifactId>
            <version>3.4</version>
        </dependency>
        
        <!-- Apache Commons io to edit the modules file -->
        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
            <version>2.5</version>
        </dependency>
    </dependencies>
    
    <build>
        <!-- Copy resources into jar -->
        <resources>
            <resource>
                <directory>${basedir}/src/main/resources</directory>
                <targetPath>resources</targetPath>
            </resource>
        </resources>
        
        <plugins>
            <!-- Maven shade plugin to generate a uber-jar -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-shade-plugin</artifactId>
                <version>2.4</version>
                <configuration>
                    <transformers>
                        <!-- Prevent License duplicates -->
                        <transformer implementation="org.apache.maven.plugins.shade.resource.ApacheLicenseResourceTransformer"> </transformer>
		  
                        <transformer implementation="org.apache.maven.plugins.shade.resource.IncludeResourceTransformer">
                            <!-- Include the README to the generated Jar -->
                            <resource>META-INF/README</resource>
                            <file>../README.txt</file>
                        </transformer>
                        
                        <!-- Configure the MANIFEST of the Jar -->
                        <transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                            <manifestEntries>
                                <!-- Add entry point -->
                                <Main-Class>org.musichands.musichands.MainMusicHands</Main-Class>
                                <!-- Add the JDK source and target entry -->
                                <X-Compile-Source-JDK>1.8</X-Compile-Source-JDK>
                                <X-Compile-Target-JDK>1.8</X-Compile-Target-JDK>
                            </manifestEntries>
                        </transformer>
                    </transformers>
                </configuration>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>shade</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
      
            <!-- Maven Jar Generator plugin -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <version>2.6</version>
            </plugin>

            <!-- Maven compiler plugin -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.3</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                </configuration>
            </plugin>

            <!-- Maven javadoc generator plugin -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>2.10.3</version>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>javadoc</goal>
                        </goals>
                        <configuration>
                            <!-- Links to external javadocs -->
                            <links>
                                <!-- javadoc 8 -->
                                <link>https://docs.oracle.com/javase/8/docs/api/</link>
                                <!-- Apache commons net -->
                                <link>https://commons.apache.org/proper/commons-io/javadocs/api-2.5/</link>
                                <!-- Apache commons io -->
                                <link>https://commons.apache.org/proper/commons-net/javadocs/api-3.5/</link>
                            </links>
                            <additionalparam>-Xdoclint:none</additionalparam>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <!-- Maven cleaner plugin -->
            <plugin>
                <artifactId>maven-clean-plugin</artifactId>
                <version>2.6.1</version>
            </plugin>
        </plugins>
    </build>
</project>
